#include <string>
#include <unordered_map>
#include <fast_io.h>
#include <fast_io_device.h>
#include <fast_io_legacy.h>

struct lc_identification
{
	std::u32string title;
	std::u32string source;
	std::u32string address;
	std::u32string contact;
	std::u32string email;
	std::u32string tel;
	std::u32string fax;
	std::u32string language;
	std::u32string territory;
	std::u32string revision;
	std::u32string date;
};

struct locale_charset
{
	std::u32string individuals;
	std::vector<std::pair<char32_t, char32_t>> ranges;
};

struct locale_symbolset
{
	std::vector<std::vector<char>> individuals;
	std::vector<std::pair<std::vector<char>, std::vector<char>>> ranges;
};

struct lc_ctype
{
	std::unordered_map<std::u32string, locale_charset> charsets;
};

struct lc_collate
{
	locale_symbolset collating_symbol;
	std::vector<std::pair<std::vector<char>, std::vector<char>>> symbol_equivalence;
};

struct lc_monetary
{
	std::u32string int_curr_symbol;
	char32_t currency_symbol;
	char32_t mon_decimal_point;
	char32_t mon_thousands_sep;
	std::size_t mon_grouping;
	char32_t positive_sign;
	char32_t negative_sign;
	std::size_t int_frac_digits;
	std::size_t frac_digits;
	std::size_t p_cs_precedes;
	std::size_t p_sep_by_space;
	std::size_t n_cs_precedes;
	std::size_t n_sep_by_space;
	std::size_t int_p_cs_precedes;
	std::size_t int_p_sep_by_space;
	std::size_t int_n_cs_precedes;
	std::size_t int_n_sep_by_space;
	std::size_t p_sign_posn;
	std::size_t n_sign_posn;
	std::size_t int_p_sign_posn;
	std::size_t int_n_sign_posn;
};

struct lc_numeric
{
	char32_t decimal_point;
	char32_t thousands_sep;
	std::string grouping;
};

struct lc_time
{
	std::u32string abday[7];
	std::u32string day[7];
	std::u32string abmon[12];
	std::u32string mon[12];
	std::u32string d_t_fmt;
	std::u32string d_fmt;
	std::u32string t_fmt;
	std::u32string t_fmt_ampm;
	std::u32string date_fmt;
	std::u32string am;
	std::u32string pm;

};

struct lc_message
{
	std::u32string yesexpr;
	std::u32string noexpr;
	std::u32string yesstr;
	std::u32string nostr;
};

struct lc_paper
{
	std::size_t width;
	std::size_t height;
};

struct lc_telephone
{
	std::u32string tel_int_fmt;
	std::u32string tel_dom_fmt;
	std::u32string int_select;
	std::u32string int_prefix;
};

struct lc_name
{
	std::u32string name_fmt;
	std::u32string name_gen;
	std::u32string name_miss;
	std::u32string name_mr;
	std::u32string name_mrs;
	std::u32string name_ms;
};

struct lc_address
{
	std::u32string postal_fmt;
	std::u32string country_name;
	std::u32string country_post;
	std::u32string country_ab2;
	std::u32string country_ab3;
	std::size_t country_num;
	std::u32string country_car;
	std::size_t country_isbn;
	std::u32string lang_name;
	std::u32string lang_ab;
	std::u32string lang_term;
	std::u32string lang_lib;
};

struct lc_measurement
{
	std::size_t measurement;
};

struct i18n
{
	lc_identification identification;
	lc_ctype ctype;
	lc_collate collate;
	lc_monetary monetary;
	lc_numeric numeric;
	lc_time time;
	lc_message message;
	lc_paper paper;
	lc_telephone telephone;
	lc_name name;
	lc_address address;
	lc_measurement measurement;
};

inline constexpr std::uint64_t hash(std::string_view str)
{
	std::uint64_t ret = 5381;

	for (auto const& ch : str)
		ret = ((ret << 5) + ret) + ch; /* hash * 33 + c */

	return ret;
}

inline constexpr char8_t fasthex2int(char8_t ch)
{
	char8_t ret1(ch - u8'0');
	if (ret1 < static_cast<char8_t>(10))
		return ret1;
	char8_t ret2(ch - u8'A');
	if (ret2 < static_cast<char8_t>(6))
		return static_cast<char8_t>(ret2 + static_cast<char8_t>(10));
	fast_io::throw_posix_error(ENOTSUP);
}

inline auto parse_arg_mixed(fast_io::istring_view<char> isv, char8_t escape_char, char8_t comment_char)
{
	enum class string_state
	{
		Default,
		InBracket,
		InQuote
	} state{ string_state::Default };
	enum class unicode_state
	{
		Default,
		ExpectingU,
		ExpectingHex
	} ustate{ unicode_state::Default };

	std::u32string cur;
	cur.reserve(8);
	std::vector<std::u32string> symbols;

	std::tuple<
		std::vector<std::u32string>,
		std::vector<std::pair<char32_t, char32_t>>,
		std::u32string
	> result;

	bool escaped(false);
	bool found_dot(false);
	char32_t curch{ 0 };

	for (char ch : fast_io::igenerator(isv))
	{
		if (escaped)
		{
			cur.push_back(ch);
			escaped = false;
			continue;
		}
		if (ch == escape_char)
		{
			escaped = true;
			continue;
		}
		if (state == string_state::Default)
		{
			if (ch == u8'\"')
			{
				state = string_state::InQuote;
				continue;
			}
			else if (ch == u8';')
			{
				if (symbols.size() == 1)
				{
					if(symbols.front().size()==1)
						std::get<2>(result).push_back(symbols.front().front());
					else
						std::get<0>(result).emplace_back(symbols.front());
				}
				else if (symbols.size() == 2) {
					std::get<1>(result).emplace_back(symbols[0].front(), symbols[1].front());
				}
				symbols.clear();
				cur.clear();
			}
			// ignore illegal characters/spaces
			switch (ustate)
			{
			case unicode_state::Default:
			{
				if (ch == u8'<')
				{
					ustate = unicode_state::ExpectingU;
					continue;
				}
			}
			break;
			case unicode_state::ExpectingU:
			{
				if (ch != u8'U')
					throw std::runtime_error("expecting U, but did not get U");
				ustate = unicode_state::ExpectingHex;
				curch = 0;
				continue;
			}
			break;
			case unicode_state::ExpectingHex:
			{
				if (ch == u8'>')
				{
					cur.push_back(curch);
					ustate = unicode_state::Default;
					symbols.emplace_back(cur);
					cur.clear();
					continue;
				}
				else
					curch = (curch << 4) | fasthex2int(ch);
			}
			}
		}
		else if (state == string_state::InQuote)
		{
			switch (ustate)
			{
			case unicode_state::Default:
			{
				if (ch == u8'\"')
				{
					state = string_state::Default;
					symbols.emplace_back(cur);
					cur.clear();
					continue;
				}
				else if (ch == u8'<')
				{
					ustate = unicode_state::ExpectingU;
					continue;
				}
				else {
					cur.push_back(ch);
					continue;
				}
			}
			break;
			case unicode_state::ExpectingU:
			{
				if (ch != u8'U')
					throw std::runtime_error("expecting U, but did not get U");
				ustate = unicode_state::ExpectingHex;
				curch = 0;
				continue;
			}
			break;
			case unicode_state::ExpectingHex:
			{
				if (ch == u8'>')
				{
					cur.push_back(curch);
					ustate = unicode_state::Default;
					continue;
				}
				else
					curch = (curch << 4) | fasthex2int(ch);
			}
			}
		}

	}

	return result;
}

template<typename T = char>
inline auto parse_arg_no_quote(fast_io::istring_view<char> isv, char8_t escape_char, char8_t comment_char)
{
	enum class string_state
	{
		Default,
		InBracket
	} state{string_state::Default};

	std::vector<T> symbol;
	symbol.reserve(8);
	std::vector<std::vector<T>> symbols;

	for (char ch : fast_io::igenerator(isv))
	{
		switch (state)
		{
		case string_state::Default:
			if (ch == u8'<')
			{
				state = string_state::InBracket;
				continue;
			}
			else if (ch == comment_char)
			{
				goto leave;
			}
			break;
		case string_state::InBracket:
			if (ch == u8'>')
			{
				symbols.emplace_back(symbol);
				symbol.clear();
				state = string_state::Default;
			}
			else {
				symbol.emplace_back(ch);
			}
			break;
		}
	}
leave:
	if (symbol.size())
		symbols.emplace_back(symbol);

	return symbols;
}

template<typename T = char32_t>
inline auto parse_arg_string(fast_io::istring_view<char> isv, char8_t escape_char)
{
	bool escaped{ false };
	enum class string_state
	{
		Default,
		InQuote,
	} state{ string_state::Default };
	enum class unicode_state
	{
		Default,
		ExpectingU,
		ExpectingHex
	} ustate{ unicode_state::Default };
	std::vector<std::basic_string<T>> result;
	result.reserve(12);
	std::basic_string<T> cur;
	cur.reserve(256);
	char32_t curch{0};
	for (char ch : fast_io::igenerator(isv))
	{
		if (escaped)
		{
			cur.push_back(ch);
			escaped = false;
			continue;
		}
		if (ch == escape_char)
		{
			escaped = true;
			continue;
		}
		if (state == string_state::Default)
		{
			if (ch == u8'\"')
			{
				state = string_state::InQuote;
				continue;
			}
			else if (ch == u8';')
			{
				result.emplace_back(cur);
				cur.clear();
			}
			// ignore illegal characters/spaces
		}
		else if (state == string_state::InQuote)
		{
			switch (ustate)
			{
			case unicode_state::Default:
			{
				if (ch == u8'\"')
				{
					state = string_state::Default;
					continue;
				}
				else if (ch == u8'<')
				{
					ustate = unicode_state::ExpectingU;
					continue;
				}
				else {
					cur.push_back(ch);
					continue;
				}
			}
			break;
			case unicode_state::ExpectingU:
			{
				if (ch != u8'U')
					throw std::runtime_error("expecting U, but did not get U");
				ustate = unicode_state::ExpectingHex;
				curch = 0;
				continue;
			}
			break;
			case unicode_state::ExpectingHex:
			{
				if (ch == u8'>')
				{
					cur.push_back(curch);
					ustate = unicode_state::Default;
					continue;
				}
				else
					curch = (curch << 4) | fasthex2int(ch);
			}
			}
		}
	}
	if (state == string_state::Default && ustate == unicode_state::Default && cur.size())
	{
		result.emplace_back(cur);
	}
	return result;
}

template<typename T>
inline constexpr bool fastisspacerev2(T ch)
{
	if (ch == 0x20)
		return true;
	std::make_unsigned_t<T> e(ch);
	e -= 9;
	return e < 5;
}

enum class locale_file_state
{
	Default,
	IDENTIFICATION,
	CTYPE,
	COLLATE,
	MONETARY,
	NUMERIC,
	TIME,
	MESSAGES,
	PAPER,
	NAME,
	ADDRESS,
	TELEPHONE,
	MEASUREMENT
};


template<fast_io::buffer_input_stream stream>
inline constexpr i18n parse_i18n_file(stream& sin, fast_io::native_io_observer iob, locale_file_state section_required = locale_file_state::Default);

inline void parse_line(i18n& i18n, std::string_view line, char8_t& comment_char, char8_t& escape_char, locale_file_state& fstate, fast_io::native_io_observer iob, locale_file_state section_required = locale_file_state::Default)
{
	std::size_t start_pos(0), end_pos(line.size());
	for (std::size_t i(0); i != line.size(); ++i)
		if (!fastisspacerev2(line[i]))
		{
			start_pos = i; break;
		}
	for (std::size_t i(start_pos); i != line.size(); ++i)
		if (fastisspacerev2(line[i])) {
			end_pos = i; break;
		}
	auto command(std::string_view(line.data() + start_pos, end_pos - start_pos));
	auto arg(std::string_view(line.data() + end_pos, line.size() - end_pos));
	fast_io::istring_view isv(arg);
	if (hash(command) == hash("END"))
		fstate = locale_file_state::Default;
	switch (fstate)
	{
	case locale_file_state::Default:
		switch (hash(command))
		{
		case hash("comment_char"):
		{
			std::size_t start_pos(0);
			for (std::size_t i(end_pos); i != line.size(); ++i)
				if (!fastisspacerev2(line[i]))
				{
					start_pos = i;
					break;
				}
			comment_char = line[start_pos];
			break;
		}
		case hash("escape_char"):
		{
			std::size_t start_pos(0);
			for (std::size_t i(end_pos); i != line.size(); ++i)
				if (!fastisspacerev2(line[i]))
				{
					start_pos = i;
					break;
				}
			escape_char = line[start_pos];
			break;
		}
		case hash("LC_IDENTIFICATION"):
			fstate = locale_file_state::IDENTIFICATION;
			break;
		case hash("LC_CTYPE"):
			fstate = locale_file_state::CTYPE;
			break;
		case hash("LC_COLLATE"):
			fstate = locale_file_state::COLLATE;
			break;
		case hash("LC_MONETARY"):
			fstate = locale_file_state::MONETARY;
			break;
		case hash("LC_NUMERIC"):
			fstate = locale_file_state::NUMERIC;
			break;
		case hash("LC_TIME"):
			fstate = locale_file_state::TIME;
			break;
		case hash("LC_MESSAGES"):
			fstate = locale_file_state::MESSAGES;
			break;
		case hash("LC_PAPER"):
			fstate = locale_file_state::PAPER;
			break;
		case hash("LC_TELEPHONE"):
			fstate = locale_file_state::TELEPHONE;
			break;
		case hash("LC_NAME"):
			fstate = locale_file_state::NAME;
			break;
		case hash("LC_ADDRESS"):
			fstate = locale_file_state::ADDRESS;
			break;
		case hash("LC_MEASUREMENT"):
			fstate = locale_file_state::MEASUREMENT;
			break;
		}
		break;
	case locale_file_state::IDENTIFICATION:
		if (section_required != locale_file_state::Default && section_required != locale_file_state::IDENTIFICATION)
			break;
		switch (hash(command))
		{
		case hash("copy"):
		{
			auto result(parse_arg_string<char>(isv, escape_char));
			auto filename(std::string(result.front().data(), result.front().size()));
			fast_io::ibuf_file ibf(at(iob), filename);
			//debug_println(fast_io::out(), "reading from ", filename);
			auto otherfile(parse_i18n_file(ibf, iob, locale_file_state::IDENTIFICATION));
			i18n.identification = otherfile.identification;
		}
		break;
		case hash("title"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.identification.title = result.front();
		}
		break;
		case hash("source"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.identification.source = result.front();
		}
		break;
		case hash("address"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.identification.address = result.front();
		}
		break;
		case hash("contact"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.identification.contact = result.front();
		}
		break;
		case hash("email"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.identification.email = result.front();
		}
		break;
		case hash("tel"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.identification.tel = result.front();
		}
		break;
		case hash("fax"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.identification.fax = result.front();
		}
		break;
		case hash("language"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.identification.language = result.front();
		}
		break;
		case hash("territory"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.identification.territory = result.front();
		}
		break;
		case hash("revision"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.identification.revision = result.front();
		}
		break;
		case hash("date"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.identification.date = result.front();
		}
		break;
		}
		break;
	case locale_file_state::CTYPE:
		if (section_required != locale_file_state::Default && section_required != locale_file_state::CTYPE)
			break;
		switch (hash(command))
		{
		case hash("copy"):
		{
			auto result(parse_arg_string<char>(isv, escape_char));
			auto filename(std::string(result.front().data(), result.front().size()));
			fast_io::ibuf_file ibf(at(iob), filename);
			//debug_println(fast_io::out(), "reading from ", filename);
			auto otherfile(parse_i18n_file(ibf, iob, locale_file_state::CTYPE));
			i18n.ctype = otherfile.ctype;
		}
		break;
		case hash("class"):
		{
			auto [strs, ranges, individuals] = parse_arg_mixed(isv, escape_char, comment_char);
			if (strs.size() == 1) {
				std::u32string name(strs.front().begin(), strs.front().end());
				i18n.ctype.charsets[name] = locale_charset{
					individuals,
					ranges
				};
			}
		}
		break;
		}
		break;
	case locale_file_state::COLLATE:
		if (section_required != locale_file_state::Default && section_required != locale_file_state::COLLATE)
			break;
		switch (hash(command))
		{
		case hash("copy"):
		{
			auto result(parse_arg_string<char>(isv, escape_char));
			auto filename(std::string(result.front().data(), result.front().size()));
			fast_io::ibuf_file ibf(at(iob), filename.c_str());
			//debug_println(fast_io::out(), "reading from ", filename);
			auto otherfile(parse_i18n_file(ibf, iob, locale_file_state::COLLATE));
			i18n.collate = otherfile.collate;
		}
		break;
		case hash("collating-symbol"):
		{
			auto result(parse_arg_no_quote<char>(isv, escape_char, comment_char));
			if (result.size() == 1) {
				i18n.collate.collating_symbol.individuals.emplace_back(result.front());
			}
			else if (result.size() == 2) {
				i18n.collate.collating_symbol.ranges.emplace_back(result[0], result[1]);
			}
			else
				fast_io::throw_posix_error(ENOTSUP);
		}
		break;
		case hash("symbol-equivalence"):
		{
			auto result(parse_arg_no_quote<char>(isv, escape_char, comment_char));
			if (result.size() == 2) {
				i18n.collate.symbol_equivalence.emplace_back(result[0], result[1]);
			}
			else
				fast_io::throw_posix_error(ENOTSUP);
		}
		break;
		}
		break;
	case locale_file_state::MONETARY:
		if (section_required != locale_file_state::Default && section_required != locale_file_state::MONETARY)
			break;
		switch (hash(command))
		{
		case hash("copy"):
		{
			auto result(parse_arg_string<char>(isv, escape_char));
			auto filename(std::string(result.front().data(), result.front().size()));
			fast_io::ibuf_file ibf(at(iob), filename.c_str());
			//debug_println(fast_io::out(), "reading from ", filename);
			auto otherfile(parse_i18n_file(ibf, iob, locale_file_state::MONETARY));
			i18n.monetary = otherfile.monetary;
		}
		break;
		case hash("int_curr_symbol"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.monetary.int_curr_symbol = result.front();
		}
		break;
		case hash("currency_symbol"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.monetary.currency_symbol = result.front().size() ? result.front().front() : 0;
			else
				i18n.monetary.currency_symbol = 0;
		}
		break;
		case hash("mon_decimal_point"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.monetary.mon_decimal_point = result.front().size() ? result.front().front() : 0;
			else
				i18n.monetary.mon_decimal_point = 0;
		}
		break;
		case hash("mon_thousands_sep"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.monetary.mon_thousands_sep = result.front().size() ? result.front().front() : 0;
			else
				i18n.monetary.mon_thousands_sep = 0;
		}
		break;
		case hash("mon_grouping"):
		{
			std::string v;
			scan(isv, v);
			i18n.monetary.mon_grouping = std::move(v);
		}
		break;
		case hash("positive_sign"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.monetary.positive_sign = result.front().size() ? result.front().front() : 0;
			else
				i18n.monetary.positive_sign = 0;
		}
		break;
		case hash("negative_sign"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.monetary.negative_sign = result.front().size() ? result.front().front() : 0;
			else
				i18n.monetary.negative_sign = 0;
		}
		break;
		case hash("int_frac_digits"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.monetary.int_frac_digits = v;
		}
		break;
		case hash("frac_digits"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.monetary.frac_digits = v;
		}
		break;
		case hash("p_cs_precedes"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.monetary.p_cs_precedes = v;
		}
		break;
		case hash("p_sep_by_space"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.monetary.p_sep_by_space = v;
		}
		break;
		case hash("n_cs_precedes"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.monetary.n_cs_precedes = v;
		}
		break;
		case hash("n_sep_by_space"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.monetary.n_sep_by_space = v;
		}
		break;
		case hash("int_p_cs_precedes"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.monetary.int_p_cs_precedes = v;
		}
		break;
		case hash("int_p_sep_by_space"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.monetary.int_p_sep_by_space = v;
		}
		break;
		case hash("int_n_cs_precedes"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.monetary.int_n_cs_precedes = v;
		}
		break;
		case hash("int_n_sep_by_space"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.monetary.int_n_sep_by_space = v;
		}
		break;
		case hash("p_sign_posn"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.monetary.p_sign_posn = v;
		}
		break;
		case hash("n_sign_posn"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.monetary.n_sign_posn = v;
		}
		break;
		case hash("int_p_sign_posn"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.monetary.int_p_sign_posn = v;
		}
		break;
		case hash("int_n_sign_posn"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.monetary.int_n_sign_posn = v;
		}
		break;
		}
		break;
	case locale_file_state::NUMERIC:
		if (section_required != locale_file_state::Default && section_required != locale_file_state::NUMERIC)
			break;
		switch (hash(command))
		{
		case hash("copy"):
		{
			auto result(parse_arg_string<char>(isv, escape_char));
			auto filename(std::string(result.front().data(), result.front().size()));
			fast_io::ibuf_file ibf(at(iob), filename.c_str());
			//debug_println(fast_io::out(), "reading from ", filename);
			auto otherfile(parse_i18n_file(ibf, iob, locale_file_state::NUMERIC));
			i18n.numeric = otherfile.numeric;
		}
		break;
		case hash("decimal_point"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.numeric.decimal_point = result.front().size() ? result.front().front() : 0;
			else
				i18n.numeric.decimal_point = 0;
		}
		break;
		case hash("thousands_sep"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.numeric.thousands_sep = result.front().size() ? result.front().front() : 0;
			else
				i18n.numeric.thousands_sep = 0;
		}
		break;
		case hash("grouping"):
		{
			std::string v;
			scan(isv, v);
			i18n.numeric.grouping = std::move(v);
		}
		break;
		}
		break;
	case locale_file_state::TIME:
		if (section_required != locale_file_state::Default && section_required != locale_file_state::TIME)
			break;
		switch (hash(command))
		{
		case hash("copy"):
		{
			auto result(parse_arg_string<char>(isv, escape_char));
			auto filename(std::string(result.front().data(), result.front().size()));
			fast_io::ibuf_file ibf(at(iob), filename.c_str());
			//debug_println(fast_io::out(), "reading from ", filename);
			auto otherfile(parse_i18n_file(ibf, iob, locale_file_state::TIME));
			i18n.time = otherfile.time;
		}
		break;
		case hash("abday"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size() != 7)
				throw std::runtime_error("incorrect number of days in a week");
			for (std::size_t i(0); i != 7; ++i)
				i18n.time.abday[i] = result[i];
		}
		break;
		case hash("day"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size() != 7)
				throw std::runtime_error("incorrect number of days in a week");
			for (std::size_t i(0); i != 7; ++i)
				i18n.time.day[i] = result[i];
		}
		break;
		case hash("abmon"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size() != 12)
				throw std::runtime_error("incorrect number of months in a year");
			for (std::size_t i(0); i != 12; ++i)
				i18n.time.abmon[i] = result[i];
		}
		break;
		case hash("mon"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size() != 12)
				throw std::runtime_error("incorrect number of months in a year");
			for (std::size_t i(0); i != 12; ++i)
				i18n.time.mon[i] = result[i];
		}
		break;
		case hash("d_t_fmt"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.time.d_t_fmt = result.front();
		}
		break;
		case hash("d_fmt"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.time.d_fmt = result.front();
		}
		break;
		case hash("t_fmt"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.time.t_fmt = result.front();
		}
		break;
		case hash("am_pm"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size() == 2)
			{
				i18n.time.am = result.front();
				i18n.time.pm = result.back();
			}
		}
		break;
		case hash("t_fmt_ampm"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.time.t_fmt_ampm = result.front();
		}
		break;
		case hash("date_fmt"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.time.date_fmt = result.front();
		}
		break;
		}
		break;
	case locale_file_state::MESSAGES:
		if (section_required != locale_file_state::Default && section_required != locale_file_state::MESSAGES)
			break;
		switch (hash(command))
		{
		case hash("copy"):
		{
			auto result(parse_arg_string<char>(isv, escape_char));
			auto filename(std::string(result.front().data(), result.front().size()));
			fast_io::ibuf_file ibf(at(iob), filename.c_str());
			//debug_println(fast_io::out(), "reading from ", filename);
			auto otherfile(parse_i18n_file(ibf, iob, locale_file_state::MESSAGES));
			i18n.message = otherfile.message;
		}
		break;
		case hash("yesexpr"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.message.yesexpr = result.front();
		}
		break;
		case hash("noexpr"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.message.noexpr = result.front();
		}
		break;
		case hash("yesstr"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.message.yesstr = result.front();
		}
		break;
		case hash("nostr"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.message.nostr = result.front();
		}
		break;
		}
		break;
	case locale_file_state::PAPER:
		if (section_required != locale_file_state::Default && section_required != locale_file_state::PAPER)
			break;
		switch (hash(command))
		{
		case hash("copy"):
		{
			auto result(parse_arg_string<char>(isv, escape_char));
			auto filename(std::string(result.front().data(), result.front().size()));
			fast_io::ibuf_file ibf(at(iob), filename.c_str());
			//debug_println(fast_io::out(), "reading from ", filename);
			auto otherfile(parse_i18n_file(ibf, iob, locale_file_state::PAPER));
			i18n.paper = otherfile.paper;
		}
		break;
		case hash("height"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.paper.height = v;
		}
		break;
		case hash("width"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.paper.width = v;
		}
		break;
		}
		break;
	case locale_file_state::NAME:
		if (section_required != locale_file_state::Default && section_required != locale_file_state::NAME)
			break;
		switch (hash(command))
		{
		case hash("copy"):
		{
			auto result(parse_arg_string<char>(isv, escape_char));
			auto filename(std::string(result.front().data(), result.front().size()));
			fast_io::ibuf_file ibf(at(iob), filename);
			//debug_println(fast_io::out(), "reading from ", filename);
			auto otherfile(parse_i18n_file(ibf, iob, locale_file_state::NAME));
			i18n.name = otherfile.name;
		}
		break;
		case hash("name_fmt"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.name.name_fmt = result.front();
		}
		break;
		case hash("name_gen"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.name.name_gen = result.front();
		}
		break;
		case hash("name_miss"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.name.name_miss = result.front();
		}
		break;
		case hash("name_mr"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.name.name_mr = result.front();
		}
		break;
		case hash("name_mrs"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.name.name_mrs = result.front();
		}
		break;
		case hash("name_ms"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.name.name_ms = result.front();
		}
		break;
		}
		break;
	case locale_file_state::ADDRESS:
		if (section_required != locale_file_state::Default && section_required != locale_file_state::ADDRESS)
			break;
		switch (hash(command))
		{
		case hash("copy"):
		{
			auto result(parse_arg_string<char>(isv, escape_char));
			auto filename(std::string(result.front().data(), result.front().size()));
			fast_io::ibuf_file ibf(fast_io::at(iob), filename);
			//debug_println(fast_io::out(), "reading from ", filename);
			auto otherfile(parse_i18n_file(ibf, iob, locale_file_state::ADDRESS));
			i18n.address = otherfile.address;
		}
		break;
		case hash("postal_fmt"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.address.postal_fmt = result.front();
		}
		break;
		case hash("country_name"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.address.country_name = result.front();
		}
		break;
		case hash("country_post"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.address.country_post = result.front();
		}
		break;
		case hash("country_ab2"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.address.country_ab2 = result.front();
		}
		break;
		case hash("country_ab3"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.address.country_ab3 = result.front();
		}
		break;
		case hash("country_num"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.address.country_num = v;
		}
		break;
		case hash("country_car"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.address.country_car = result.front();
		}
		break;
		case hash("country_isbn"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.address.country_isbn = v;
		}
		break;
		case hash("lang_name"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.address.lang_name = result.front();
		}
		break;
		case hash("lang_ab"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.address.lang_ab = result.front();
		}
		break;
		case hash("lang_term"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.address.lang_term = result.front();
		}
		break;
		case hash("lang_lib"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.address.lang_lib = result.front();
		}
		break;
		}
		break;
	case locale_file_state::TELEPHONE:
		if (section_required != locale_file_state::Default && section_required != locale_file_state::TELEPHONE)
			break;
		switch (hash(command))
		{
		case hash("copy"):
		{
			auto result(parse_arg_string<char>(isv, escape_char));
			auto filename(std::string(result.front().data(), result.front().size()));
			fast_io::ibuf_file ibf(at(iob), filename);
			//debug_println(fast_io::out(), "reading from ", filename);
			auto otherfile(parse_i18n_file(ibf, iob, locale_file_state::TELEPHONE));
			i18n.telephone = otherfile.telephone;
		}
		break;
		case hash("tel_int_fmt"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.telephone.tel_int_fmt = result.front();
		}
		break;
		case hash("tel_dom_fmt"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.telephone.tel_dom_fmt = result.front();
		}
		break;
		case hash("int_select"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.telephone.int_select = result.front();
		}
		break;
		case hash("int_prefix"):
		{
			auto result(parse_arg_string(isv, escape_char));
			if (result.size())
				i18n.telephone.int_prefix = result.front();
		}
		break;
		}
		break;
	case locale_file_state::MEASUREMENT:
		if (section_required != locale_file_state::Default && section_required != locale_file_state::MEASUREMENT)
			break;
		switch (hash(command))
		{
		case hash("copy"):
		{
			auto result(parse_arg_string<char>(isv, escape_char));
			auto filename(std::string(result.front().data(), result.front().size()));
			fast_io::ibuf_file ibf(at(iob), filename.c_str());
			//debug_println(fast_io::out(), "reading from ", filename);
			auto otherfile(parse_i18n_file(ibf, iob, locale_file_state::MEASUREMENT));
			i18n.measurement = otherfile.measurement;
		}
		break;
		case hash("measurement"):
		{
			std::size_t v{ 0 };
			scan(isv, v);
			i18n.measurement.measurement = v;
		}
		break;
		}
	}
}

template<fast_io::buffer_input_stream stream>
inline constexpr i18n parse_i18n_file(stream& sin, fast_io::native_io_observer iob, locale_file_state section_required)
{
	i18n result;
	locale_file_state fstate{ locale_file_state::Default };
	std::vector<char> line;
	char8_t comment_char{ u8'\0' }, escape_char{ u8'\0' };
	bool line_continue{ false };
	line.reserve(256);
	for (auto curline : fast_io::line_generator(sin)) {
		if (curline.size() && curline.front() != comment_char) {
			if (line_continue) {
				line.insert(line.end(), curline.begin(), curline.end() - (curline.back() == escape_char));
				line.emplace_back('\n');
			}
			else {
				if (line.size()) {
					parse_line(result, std::string_view(line.data(), line.size()), comment_char, escape_char, fstate, iob, section_required);
				}
				line.assign(curline.begin(), curline.end() - (curline.back() == escape_char));
				line.emplace_back('\n');
			}
			line_continue = curline.back() == escape_char;
		}
	}
	if (line.size()) {
		parse_line(result, std::string_view(line.data(), line.size()), comment_char, escape_char, fstate, iob, section_required);
	}
	return result;
}

int main()
{
	fast_io::dir_file df("localedata");
	fast_io::ibuf_file ibf(at(df),"zh_CN");
	auto i18n{parse_i18n_file(ibf,df,locale_file_state::Default)};
	fast_io::dir_file df2("output");
	fast_io::obuf_file obf(at(df2),"zh_CN.cc");
	print(obf,R"abc(#include<stddef.h>

namespace fast_io
{

inline constexpr u8_lc_all u8_lc_all_data =
)abc");

	auto lmd([&](std::u32string_view u32_view){
		for(auto ch: u32_view)
			print(obf,"0x",ch,",");
	});
	lmd();
	print(,R"abc()};
}

)abc");
}

