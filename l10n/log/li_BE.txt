category: LC_TIME
week -> [3]
	7
	19971130
	4
am_pm -> [2]
	""
	""
t_fmt -> "%T"
first_weekday -> 2
d_fmt -> "%d.%m.%Y"
date_fmt -> "%a %d. %b %Y %T %Z"
mon -> [12]
	"jannewarie"
	"fibberwarie"
	"miert"
	"eprèl"
	"meij"
	"junie"
	"julie"
	"augustus"
	"september"
	"oktober"
	"november"
	"desember"
abmon -> [12]
	"jan"
	"fib"
	"mie"
	"epr"
	"mei"
	"jun"
	"jul"
	"aug"
	"sep"
	"okt"
	"nov"
	"des"
d_t_fmt -> "%a %d. %b %Y %T"
day -> [7]
	"zóndig"
	"maondig"
	"daensdig"
	"goonsdig"
	"dónderdig"
	"vriedig"
	"zaoterdig"
t_fmt_ampm -> ""
abday -> [7]
	"zón"
	"mao"
	"dae"
	"goo"
	"dón"
	"vri"
	"zao"

category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_select -> "00"
int_prefix -> "32"
tel_int_fmt -> "+%c %a %l"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> "."
decimal_point -> ","

category: LC_NAME
name_mrs -> "mevrouw"
name_miss -> "mevrouw"
name_mr -> "heer"
name_ms -> "mevrouw"
name_gen -> "heer of mevrouw"
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "EUR "
int_frac_digits -> 2
currency_symbol -> "€"
p_cs_precedes -> 1
p_sep_by_space -> 1
mon_decimal_point -> ","
n_sign_posn -> 4
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> "."
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 2

category: LC_MESSAGES
nostr -> "nee"
yesstr -> "ja"
noexpr -> "^[-0nN]"
yesexpr -> "^[+1jJyY]"

category: LC_PAPER
width -> 210
height -> 297

category: LC_ADDRESS
lang_lib -> "lim"
lang_ab -> "li"
country_num -> 056
lang_term -> "lim"
lang_name -> "Lèmbörgs"
country_car -> "B"
country_ab3 -> "BEL"
country_post -> "B"
country_ab2 -> "BE"
country_name -> "Bèlsj"
postal_fmt -> "%f%N%a%N%d%N%b%N%s %h %e %r%N%z %T%N%c%N"

category: LC_IDENTIFICATION
revision -> "0.1"
date -> "2003-11-30"
territory -> "Belgium"
email -> "kenneth@gnu.org, pablo@mandriva.com"
tel -> ""
contact -> "Kenneth Christiansen, Pablo Saratxaga"
language -> "Limburgish"
address -> ""
source -> "information from Kenneth Christiansen"
title -> "Limburgish Language Locale for Belgium"
