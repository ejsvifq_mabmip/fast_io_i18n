category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_select -> "00"
int_prefix -> "91"
tel_int_fmt -> "+%c ;%a ;%l"

category: LC_ADDRESS
lang_lib -> "kas"
lang_ab -> "ks"
lang_term -> "kas"
lang_name -> "कॉशुर"
country_car -> "IND"
country_num -> 356
country_ab3 -> "IND"
country_ab2 -> "IN"
country_name -> "भारत"
postal_fmt -> "%z%c%T%s%b%e%r"

category: LC_PAPER
width -> 210
height -> 297

category: LC_MESSAGES
nostr -> "न"
yesstr -> "इंन"
noexpr -> "^[-0nNन]"
yesexpr -> "^[+1yYइ]"

category: LC_TIME
week -> [3]
	7
	19971130
	1
t_fmt -> "%I:%M:%S %p %Z"
d_fmt -> "%-m//%-d//%y"
date_fmt -> "%A %d %b %Y %I:%M:%S %p %Z"
am_pm -> [2]
	"पूर्वाह्न"
	"अपराह्न"
mon -> [12]
	"जनवरी"
	"फ़रवरी"
	"मार्च"
	"अप्रेल"
	"मई"
	"जून"
	"जुलाई"
	"अगस्त"
	"सितम्बर"
	"अक्टूबर"
	"नवम्बर"
	"दिसम्बर"
abmon -> [12]
	"जनवरी"
	"फ़रवरी"
	"मार्च"
	"अप्रेल"
	"मई"
	"जून"
	"जुलाई"
	"अगस्त"
	"सितम्बर"
	"अक्टूबर"
	"नवम्बर"
	"दिसम्बर"
d_t_fmt -> "%A %d %b %Y %I:%M:%S %p"
day -> [7]
	"आथवार"
	"चॅ़दुरवार"
	"बोमवार"
	"ब्वदवार"
	"ब्रसवार"
	"शोकुरवार"
	"बटुवार"
t_fmt_ampm -> "%I:%M:%S %p %Z"
abday -> [7]
	"आथ "
	"चॅ़दुर"
	"बोम"
	"ब्वद"
	"ब्रस"
	"शोकुर"
	"बटु"

category: LC_NAME
name_mrs -> "श्रीमती."
name_miss -> "कुमारी."
name_mr -> "श्री."
name_ms -> "कुमार."
name_gen -> ""
name_fmt -> "%p%t%f%t%g"

category: LC_NUMERIC
grouping -> 3
thousands_sep -> ","
decimal_point -> "."

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "INR "
int_frac_digits -> 2
currency_symbol -> "₹"
p_cs_precedes -> 1
p_sep_by_space -> 0
mon_decimal_point -> "."
n_sign_posn -> 1
mon_grouping -> [2]
	3
	2
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 0

category: LC_IDENTIFICATION
revision -> "0.1"
date -> "2008-08-26"
territory -> "India"
fax -> ""
email -> "ks-gnome-trans-commits@lists.code.indlinux.net"
tel -> ""
contact -> ""
language -> "Kashmiri"
address -> ""
source -> ""
title -> "Kashmiri(devanagari) language locale for India"
