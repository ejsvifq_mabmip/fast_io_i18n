category: LC_MEASUREMENT
measurement -> 2

category: LC_TELEPHONE
int_prefix -> "1"
int_select -> "11"
tel_dom_fmt -> "(%a) %l"
tel_int_fmt -> "+%c (%a) %l"

category: LC_ADDRESS
lang_lib -> "chr"
lang_ab -> ""
country_isbn -> 0
lang_term -> "chr"
lang_name -> "ᏣᎳᎩ"
country_car -> "USA"
country_num -> 840
country_ab3 -> "USA"
country_post -> "USA"
country_ab2 -> "US"
country_name -> "ᏌᏊ ᎢᏳᎾᎵᏍᏔᏅ ᏍᎦᏚᎩ"
postal_fmt -> "%a%N%f%N%d%N%b%N%h %s %e %r%N%T, %S %z%N%c%N"

category: LC_PAPER
width -> 216
height -> 279

category: LC_MESSAGES
nostr -> "ᎥᏝ"
yesstr -> "ᎥᎥ"
noexpr -> "^([-0nN]|ᎥᏝ)"
yesexpr -> "^([+1yY]|ᎥᎥ)"

category: LC_TIME
week -> [3]
	7
	19971130
	1
am_pm -> [2]
	"ᏌᎾᎴ"
	"ᏒᎯᏱᎢᏗᏢ"
t_fmt -> "%r"
d_fmt -> "%m//%d//%Y"
date_fmt -> "%a %d %b %Y %r %Z"
mon -> [12]
	"ᎤᏃᎸᏔᏅ"
	"ᎧᎦᎵ"
	"ᎠᏅᏱ"
	"ᎧᏬᏂ"
	"ᎠᏂᏍᎬᏘ"
	"ᏕᎭᎷᏱ"
	"ᎫᏰᏉᏂ"
	"ᎦᎶᏂ"
	"ᏚᎵᏍᏗ"
	"ᏚᏂᏅᏗ"
	"ᏅᏓᏕᏆ"
	"ᎥᏍᎩᏱ"
abmon -> [12]
	"ᎤᏃ"
	"ᎧᎦ"
	"ᎠᏅ"
	"ᎧᏬ"
	"ᎠᏂ"
	"ᏕᎭ"
	"ᎫᏰ"
	"ᎦᎶ"
	"ᏚᎵ"
	"ᏚᏂ"
	"ᏅᏓ"
	"ᎥᏍ"
d_t_fmt -> "%a %d %b %Y %r"
day -> [7]
	"ᎤᎾᏙᏓᏆᏍᎬ"
	"ᎤᎾᏙᏓᏉᏅᎯ"
	"ᏔᎵᏁᎢᎦ"
	"ᏦᎢᏁᎢᎦ"
	"ᏅᎩᏁᎢᎦ"
	"ᏧᎾᎩᎶᏍᏗ"
	"ᎤᎾᏙᏓᏈᏕᎾ"
t_fmt_ampm -> "%I:%M:%S %p"
abday -> [7]
	"ᏆᏍᎬ"
	"ᏉᏅᎯ"
	"ᏔᎵᏁ"
	"ᏦᎢᏁ"
	"ᏅᎩᏁ"
	"ᏧᎾᎩ"
	"ᏈᏕᎾ"

category: LC_NAME
name_ms -> "Ms."
name_mrs -> "Mrs."
name_mr -> "Mr."
name_miss -> "Miss."
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> ","
decimal_point -> "."

category: LC_MONETARY
p_sign_posn -> 1
n_sep_by_space -> 0
int_curr_symbol -> "USD "
int_frac_digits -> 2
int_n_sep_by_space -> 1
currency_symbol -> "$"
p_cs_precedes -> 1
p_sep_by_space -> 0
mon_decimal_point -> "."
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
int_p_sep_by_space -> 1

category: LC_IDENTIFICATION
revision -> "1.0"
date -> "2010-10-20"
territory -> "United States"
fax -> ""
email -> "josepherb7@gmail.com"
tel -> ""
contact -> "Joseph Erb"
language -> "Cherokee"
address -> "http:////cldr.unicode.org//index//process"
source -> "Cherokee Nation"
title -> "Cherokee language locale for United States"
