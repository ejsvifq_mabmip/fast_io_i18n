category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_select -> "00"
int_prefix -> "353"
tel_int_fmt -> "+%c %a %l"

category: LC_ADDRESS
lang_lib -> "gle"
lang_ab -> "ga"
lang_term -> "gle"
lang_name -> "Gaeilge"
country_car -> "IRL"
country_num -> 372
country_ab3 -> "IRL"
country_ab2 -> "IE"
country_name -> "Éire"
postal_fmt -> "%f%N%a%N%d%N%b%N%s %h %e %r%N%z %T%N%c%N"

category: LC_PAPER
width -> 210
height -> 297

category: LC_MESSAGES
nostr -> "níl"
yesstr -> "tá"
noexpr -> "^[-0nN]"
yesexpr -> "^[+1tTyY]"

category: LC_TIME
week -> [3]
	7
	19971130
	4
am_pm -> [2]
	""
	""
t_fmt -> "%T"
first_weekday -> 2
d_fmt -> "%d.%m.%y"
date_fmt -> "%a %d %b %Y %T %Z"
mon -> [12]
	"Eanáir"
	"Feabhra"
	"Márta"
	"Aibreán"
	"Bealtaine"
	"Meitheamh"
	"Iúil"
	"Lúnasa"
	"Meán Fómhair"
	"Deireadh Fómhair"
	"Samhain"
	"Nollaig"
abmon -> [12]
	"Ean"
	"Feabh"
	"Márta"
	"Aib"
	"Beal"
	"Meith"
	"Iúil"
	"Lún"
	"MFómh"
	"DFómh"
	"Samh"
	"Noll"
d_t_fmt -> "%a %d %b %Y %T"
day -> [7]
	"Dé Domhnaigh"
	"Dé Luain"
	"Dé Máirt"
	"Dé Céadaoin"
	"Déardaoin"
	"Dé hAoine"
	"Dé Sathairn"
t_fmt_ampm -> ""
abday -> [7]
	"Domh"
	"Luan"
	"Máirt"
	"Céad"
	"Déar"
	"Aoine"
	"Sath"

category: LC_NAME
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> ","
decimal_point -> "."

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "EUR "
int_frac_digits -> 2
currency_symbol -> "€"
p_cs_precedes -> 1
p_sep_by_space -> 0
mon_decimal_point -> "."
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 0

category: LC_IDENTIFICATION
revision -> "1.0"
date -> "2000-06-29"
territory -> "Ireland"
fax -> ""
email -> "bug-glibc-locales@gnu.org"
tel -> ""
contact -> ""
language -> "Irish"
address -> "Glasnevin, Dublin 9, Ireland"
source -> "NSAI"
title -> "Irish locale for Ireland"
