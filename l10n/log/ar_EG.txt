category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_select -> "00"
int_prefix -> "20"
tel_int_fmt -> "+%c ;%a ;%l"

category: LC_ADDRESS
lang_lib -> "ara"
lang_ab -> "ar"
lang_term -> "ara"
lang_name -> "العربية"
country_car -> "ET"
country_num -> 818
country_ab3 -> "EGY"
country_ab2 -> "EG"
country_name -> "مصر"
postal_fmt -> "%z%c%T%s%b%e%r"

category: LC_PAPER
width -> 210
height -> 297

category: LC_MESSAGES
nostr -> "لا"
yesstr -> "نعم"
noexpr -> "^[-0لnN]"
yesexpr -> "^[+1نyY]"

category: LC_TIME
first_workday -> 1
week -> [3]
	7
	19971130
	1
t_fmt -> "%Z %I:%M:%S %p"
first_weekday -> 7
d_fmt -> "%d %b, %Y"
date_fmt -> "%d %b, %Y %Z %I:%M:%S %p"
am_pm -> [2]
	"ص"
	"م"
mon -> [12]
	"يناير"
	"فبراير"
	"مارس"
	"أبريل"
	"مايو"
	"يونيو"
	"يوليو"
	"أغسطس"
	"سبتمبر"
	"أكتوبر"
	"نوفمبر"
	"ديسمبر"
abmon -> [12]
	"ينا"
	"فبر"
	"مار"
	"أبر"
	"ماي"
	"يون"
	"يول"
	"أغس"
	"سبت"
	"أكت"
	"نوف"
	"ديس"
d_t_fmt -> "%d %b, %Y %I:%M:%S %p"
day -> [7]
	"الأحد"
	"الاثنين"
	"الثلاثاء"
	"الأربعاء"
	"الخميس"
	"الجمعة"
	"السبت"
t_fmt_ampm -> "%Z %I:%M:%S %p"
abday -> [7]
	"ح"
	"ن"
	"ث"
	"ر"
	"خ"
	"ج"
	"س"

category: LC_NAME
name_mrs -> "Mrs."
name_miss -> "Miss."
name_mr -> "Mr."
name_ms -> "Ms."
name_gen -> "-san"
name_fmt -> "%p%t%f%t%g"

category: LC_NUMERIC
grouping -> 3
thousands_sep -> ","
decimal_point -> "."

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "EGP "
int_frac_digits -> 3
currency_symbol -> "ج.م."
p_cs_precedes -> 1
p_sep_by_space -> 1
mon_decimal_point -> "."
n_sign_posn -> 2
mon_grouping -> 3
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 3
n_sep_by_space -> 1

category: LC_IDENTIFICATION
revision -> "1.0"
date -> "2000-07-20"
territory -> "Egypt"
fax -> ""
email -> "bug-glibc-locales@gnu.org"
tel -> ""
contact -> ""
language -> "Arabic"
address -> "1623-14, Shimotsuruma, Yamato-shi, Kanagawa-ken, 242-8502, Japan"
source -> "IBM Globalization Center of Competency, Yamato Software Laboratory"
title -> "Arabic language locale for Egypt"
