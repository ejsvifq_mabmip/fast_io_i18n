category: LC_TELEPHONE
int_prefix -> "254"
int_select -> "000"
tel_dom_fmt -> "%A %l"
tel_int_fmt -> "%c %a %l"

category: LC_MESSAGES
nostr -> "Hapana"
yesstr -> "Ndiyo"
noexpr -> "^[-0hHlL]"
yesexpr -> "^[+1nNyY]"

category: LC_MEASUREMENT
measurement -> 1

category: LC_ADDRESS
lang_lib -> "swa"
lang_ab -> "sw"
lang_term -> "swa"
lang_name -> "Kiswahili"
country_car -> "EAK"
country_num -> 404
country_ab3 -> "KEN"
country_post -> "KE"
country_ab2 -> "KE"
country_name -> "Kenya"
postal_fmt -> "%a%N%f%N%d%N%b%N%h %s %e %r%N%T, %S %z%N%c%N"

category: LC_PAPER
width -> 210
height -> 297

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "KES "
int_frac_digits -> 2
currency_symbol -> "Ksh"
p_cs_precedes -> 1
p_sep_by_space -> 0
mon_decimal_point -> "."
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 0

category: LC_NAME
name_ms -> "Bi."
name_mr -> "Bw."
name_fmt -> "%p%t%g%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> ","
decimal_point -> "."

category: LC_TIME
am_pm -> [2]
	"asubuhi"
	"alasiri"
t_fmt -> "%I:%M:%S %p"
d_fmt -> "%d//%m//%Y"
date_fmt -> "%e %B %Y %I:%M:%S %p %Z"
week -> [3]
	7
	19971130
	1
t_fmt_ampm -> "%I:%M:%S %p"
abday -> [7]
	"J2"
	"J3"
	"J4"
	"J5"
	"Alh"
	"Ij"
	"J1"
d_t_fmt -> "%e %B %Y %I:%M:%S %p"
day -> [7]
	"Jumapili"
	"Jumatatu"
	"Jumanne"
	"Jumatano"
	"Alhamisi"
	"Ijumaa"
	"Jumamosi"
abmon -> [12]
	"Jan"
	"Feb"
	"Mac"
	"Apr"
	"Mei"
	"Jun"
	"Jul"
	"Ago"
	"Sep"
	"Okt"
	"Nov"
	"Des"
mon -> [12]
	"Januari"
	"Februari"
	"Machi"
	"Aprili"
	"Mei"
	"Juni"
	"Julai"
	"Agosti"
	"Septemba"
	"Oktoba"
	"Novemba"
	"Desemba"

category: LC_IDENTIFICATION
revision -> "1.0"
date -> "2011-03-07"
territory -> "Kenya"
fax -> ""
email -> "locales@kamusi.org"
tel -> ""
contact -> "Martin Benjamin"
language -> "Swahili"
source -> "Kamusi Project"
title -> "Swahili locale for Kenya"
