category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_select -> "00"
int_prefix -> "51"
tel_int_fmt -> "+%c %a %l"

category: LC_ADDRESS
lang_lib -> "spa"
lang_ab -> "es"
lang_term -> "spa"
lang_name -> "español"
country_car -> "PE"
country_num -> 604
country_ab3 -> "PER"
country_ab2 -> "PE"
country_name -> "Perú"
postal_fmt -> "%f%N%a%N%d%N%b%N%s %h %e %r%N%z %T%N%c%N"

category: LC_PAPER
width -> 210
height -> 297

category: LC_TIME
week -> [3]
	7
	19971130
	1
am_pm -> [2]
	"AM"
	"PM"
t_fmt -> "%T"
d_fmt -> "%d//%m//%y"
date_fmt -> "%a %d %b %Y %T %Z"
mon -> [12]
	"enero"
	"febrero"
	"marzo"
	"abril"
	"mayo"
	"junio"
	"julio"
	"agosto"
	"setiembre"
	"octubre"
	"noviembre"
	"diciembre"
abmon -> [12]
	"ene"
	"feb"
	"mar"
	"abr"
	"may"
	"jun"
	"jul"
	"ago"
	"set"
	"oct"
	"nov"
	"dic"
d_t_fmt -> "%a %d %b %Y %T"
day -> [7]
	"domingo"
	"lunes"
	"martes"
	"miércoles"
	"jueves"
	"viernes"
	"sábado"
t_fmt_ampm -> "%I:%M:%S %p"
abday -> [7]
	"dom"
	"lun"
	"mar"
	"mié"
	"jue"
	"vie"
	"sáb"

category: LC_NAME
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> "."
decimal_point -> ","

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "PEN "
int_frac_digits -> 2
currency_symbol -> "S//"
p_cs_precedes -> 1
p_sep_by_space -> 1
mon_decimal_point -> "."
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 1

category: LC_MESSAGES
nostr -> "no"
yesstr -> "sí"
noexpr -> "^[-0nN]"
yesexpr -> "^[+1sSyY]"

category: LC_IDENTIFICATION
revision -> "1.0"
date -> "2000-06-29"
territory -> "Peru"
fax -> ""
email -> "bug-glibc-locales@gnu.org"
tel -> ""
contact -> ""
language -> "Spanish"
address -> "Sankt Jørgens Alle 8, DK-1615 København V, Danmark"
source -> "RAP"
title -> "Spanish locale for Peru"
