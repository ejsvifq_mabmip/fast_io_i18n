category: LC_MESSAGES
nostr -> "קײן"
yesstr -> "יאָ"
noexpr -> "^[-0nNנק]"
yesexpr -> "^[+1yYי]"

category: LC_TIME
week -> [3]
	7
	19971130
	1
cal_direction -> 3
am_pm -> [2]
	"AM"
	"PM"
t_fmt -> "%H:%M:%S"
d_fmt -> "%d//%m//%y"
date_fmt -> "%Z %H:%M:%S %Y %b %d %a"
abmon -> [12]
	"יאַנ"
	"פֿעב"
	"מאַר"
	"אַפּר"
	"מײַ "
	"יונ"
	"יול"
	"אױג"
	"סעפּ"
	"אָקט"
	"נאָװ"
	"דעצ"
mon -> [12]
	"יאַנואַר"
	"פֿעברואַר"
	"מערץ"
	"אַפּריל"
	"מיי"
	"יוני"
	"יולי"
	"אויגוסט"
	"סעפּטעמבער"
	"אקטאבער"
	"נאוועמבער"
	"דעצעמבער"
d_t_fmt -> "%H:%M:%S %Y %b %d %a"
day -> [7]
	"זונטיק"
	"מאָנטיק"
	"דינסטיק"
	"מיטװאָך"
	"דאָנערשטיק"
	"פֿרײַטיק"
	"שבת"
t_fmt_ampm -> "%I:%M:%S %P"
abday -> [7]
	"זונ'"
	"מאָנ'"
	"דינ'"
	"מיט'"
	"דאָנ'"
	"פֿרײַ'"
	"שבת"

category: LC_NAME
name_ms -> "Ms."
name_mrs -> "Mrs."
name_mr -> "Mr."
name_miss -> "Miss."
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> ","
decimal_point -> "."

category: LC_MEASUREMENT
measurement -> 2

category: LC_TELEPHONE
int_prefix -> "1"
int_select -> "11"
tel_dom_fmt -> "(%a) %l"
tel_int_fmt -> "+%c (%a) %l"

category: LC_MONETARY
p_sign_posn -> 2
int_curr_symbol -> "USD "
int_frac_digits -> 2
currency_symbol -> "$"
p_cs_precedes -> 1
p_sep_by_space -> 1
mon_decimal_point -> "."
n_sign_posn -> 2
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 1

category: LC_PAPER
width -> 216
height -> 279

category: LC_ADDRESS
lang_lib -> "yid"
lang_ab -> "yi"
country_isbn -> "0"
lang_term -> "yid"
lang_name -> "ייִדיש"
country_car -> "USA"
country_num -> 840
country_ab3 -> "USA"
country_post -> "USA"
country_ab2 -> "US"
country_name -> "פֿאַראייניגטע שטאַטן"
postal_fmt -> "%d%N%f%N%d%N%b%N%s %h 5e %r%N%C%z %T%N%c%N"

category: LC_IDENTIFICATION
revision -> "0.4"
date -> "2003-08-16"
territory -> "United States"
fax -> ""
email -> "pablo@mandrakesoft.com"
tel -> ""
contact -> "Pablo Saratxaga"
language -> "Yiddish"
address -> ""
source -> "http:////www.uyip.org//"
title -> "Yiddish Language locale for the USA"
