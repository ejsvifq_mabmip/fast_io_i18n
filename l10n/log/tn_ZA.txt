category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_prefix -> "27"
int_select -> "00"
tel_dom_fmt -> "(%A) %l"
tel_int_fmt -> "+%c %a %l"

category: LC_ADDRESS
country_num -> 710
postal_fmt -> "%f%N%a%N%d%N%b%N%s %h %e %r%N%z %T%N%c%N"
lang_lib -> "tsn"
lang_ab -> "tn"
lang_term -> "tsn"
lang_name -> "Setswana"
country_car -> "ZA"
country_ab3 -> "ZAF"
country_post -> "ZA"
country_ab2 -> "ZA"
country_name -> "Aforika Borwa"

category: LC_PAPER
width -> 210
height -> 297

category: LC_MESSAGES
noexpr -> "^[-0nN]"
yesexpr -> "^[+1yYeE]"

category: LC_TIME
week -> [3]
	7
	19971130
	1
am_pm -> [2]
	""
	""
t_fmt -> "%T"
date_fmt -> "%a %b %-e %H:%M:%S %Z %Y"
d_fmt -> "%d//%m//%Y"
mon -> [12]
	"Ferikgong"
	"Tlhakole"
	"Mopitlwe"
	"Moranang"
	"Motsheganong"
	"Seetebosigo"
	"Phukwi"
	"Phatwe"
	"Lwetse"
	"Diphalane"
	"Ngwanatsele"
	"Sedimonthole"
abmon -> [12]
	"Fer"
	"Tlh"
	"Mop"
	"Mor"
	"Mot"
	"See"
	"Phu"
	"Pha"
	"Lwe"
	"Dip"
	"Ngw"
	"Sed"
d_t_fmt -> "%a %-e %b %Y %T %Z"
day -> [7]
	"laTshipi"
	"Mosupologo"
	"Labobedi"
	"Laboraro"
	"Labone"
	"Labotlhano"
	"Lamatlhatso"
t_fmt_ampm -> ""
abday -> [7]
	"Tsh"
	"Mos"
	"Bed"
	"Rar"
	"Ne"
	"Tlh"
	"Mat"

category: LC_NAME
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> ","
decimal_point -> "."

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "ZAR "
int_frac_digits -> 2
currency_symbol -> "R"
p_cs_precedes -> 1
p_sep_by_space -> 0
mon_decimal_point -> "."
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 0

category: LC_IDENTIFICATION
revision -> "0.4"
date -> "2005-10-13"
territory -> "South Africa"
fax -> ""
email -> "dwayne@translate.org.za"
tel -> ""
contact -> "Dwayne Bailey"
language -> "Tswana"
address -> "PO Box 28364, Sunnyside, 0132, South Africa"
source -> "Zuza Software Foundation (Translate.org.za)"
title -> "Tswana locale for South Africa"
