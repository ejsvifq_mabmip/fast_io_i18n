category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_select -> "00"
int_prefix -> "41"
tel_int_fmt -> "+%c %a %l"

category: LC_ADDRESS
lang_lib -> "ger"
lang_ab -> "de"
lang_term -> "deu"
lang_name -> "Deutsch"
country_car -> "CH"
country_num -> 756
country_ab3 -> "CHE"
country_ab2 -> "CH"
country_name -> "Schweiz"
postal_fmt -> "%f%N%a%N%d%N%b%N%s %h %e %r%N%z %T%N%c%N"

category: LC_PAPER
width -> 210
height -> 297

category: LC_TIME
week -> [3]
	7
	19971130
	4
am_pm -> [2]
	""
	""
t_fmt -> "%T"
first_weekday -> 2
d_fmt -> "%d.%m.%Y"
date_fmt -> "%a %d %b %Y %T %Z"
mon -> [12]
	"Januar"
	"Februar"
	"März"
	"April"
	"Mai"
	"Juni"
	"Juli"
	"August"
	"September"
	"Oktober"
	"November"
	"Dezember"
abmon -> [12]
	"Jan"
	"Feb"
	"Mär"
	"Apr"
	"Mai"
	"Jun"
	"Jul"
	"Aug"
	"Sep"
	"Okt"
	"Nov"
	"Dez"
d_t_fmt -> "%a %d %b %Y %T"
day -> [7]
	"Sonntag"
	"Montag"
	"Dienstag"
	"Mittwoch"
	"Donnerstag"
	"Freitag"
	"Samstag"
t_fmt_ampm -> ""
abday -> [7]
	"So"
	"Mo"
	"Di"
	"Mi"
	"Do"
	"Fr"
	"Sa"

category: LC_NAME
name_ms -> "Frau"
name_mrs -> "Frau"
name_mr -> "Herr"
name_miss -> "Fräulein"
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> "’"
decimal_point -> "."

category: LC_MONETARY
p_sign_posn -> 4
int_curr_symbol -> "CHF "
int_frac_digits -> 2
currency_symbol -> "CHF"
p_cs_precedes -> 1
p_sep_by_space -> 1
mon_decimal_point -> "."
n_sign_posn -> 4
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> "’"
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 1

category: LC_MESSAGES
nostr -> "nein"
yesstr -> "ja"
noexpr -> "^[-0nN]"
yesexpr -> "^[+1jJyY]"

category: LC_IDENTIFICATION
revision -> "1.0"
date -> "2007-09-23"
territory -> "Switzerland"
fax -> ""
email -> "bug-glibc-locales@gnu.org"
tel -> ""
contact -> ""
language -> "Swiss High German"
address -> "Sankt Jørgens Alle 8, DK-1615 København V, Danmark"
source -> "RAP"
title -> "German locale for Switzerland"
