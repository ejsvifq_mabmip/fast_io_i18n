category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_select -> "00"
int_prefix -> "45"
tel_int_fmt -> "+%c %a %l"

category: LC_ADDRESS
lang_lib -> "eng"
lang_ab -> "en"
lang_term -> "eng"
lang_name -> "English"
country_car -> "DK"
country_num -> 208
country_ab3 -> "DNK"
country_ab2 -> "DK"
country_name -> "Denmark"
postal_fmt -> "%f%N%a%N%d%N%b%N%s %h %e %r%N%z %T%N%c%N"

category: LC_PAPER
width -> 210
height -> 297

category: LC_MESSAGES
nostr -> "no"
yesstr -> "yes"
noexpr -> "^[-0nN]"
yesexpr -> "^[+1yYjJsSoO]"

category: LC_TIME
week -> [3]
	7
	19971130
	4
am_pm -> [2]
	""
	""
t_fmt -> "%T"
first_weekday -> 2
d_fmt -> "%Y-%m-%d"
date_fmt -> "%Y-%m-%dT%T %Z"
mon -> [12]
	"January"
	"February"
	"March"
	"April"
	"May"
	"June"
	"July"
	"August"
	"September"
	"October"
	"November"
	"December"
abmon -> [12]
	"Jan"
	"Feb"
	"Mar"
	"Apr"
	"May"
	"Jun"
	"Jul"
	"Aug"
	"Sep"
	"Oct"
	"Nov"
	"Dec"
d_t_fmt -> "%Y-%m-%dT%T %Z"
day -> [7]
	"Sunday"
	"Monday"
	"Tuesday"
	"Wednesday"
	"Thursday"
	"Friday"
	"Saturday"
t_fmt_ampm -> ""
abday -> [7]
	"Sun"
	"Mon"
	"Tue"
	"Wed"
	"Thu"
	"Fri"
	"Sat"

category: LC_NAME
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> "."
decimal_point -> ","

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "DKK "
int_frac_digits -> 2
currency_symbol -> "kr."
p_cs_precedes -> 1
p_sep_by_space -> 0
mon_decimal_point -> ","
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> "."
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 0

category: LC_IDENTIFICATION
revision -> "1.0"
date -> "2000-06-29"
territory -> "Denmark"
fax -> ""
email -> "bug-glibc-locales@gnu.org"
tel -> ""
contact -> ""
language -> "English"
address -> "Kollegievej 6, DK-2920 Charlottenlund, Danmark"
source -> "Danish Standards Association"
title -> "English locale for Denmark"
