category: LC_TIME
week -> [3]
	7
	19971130
	1
t_fmt -> "%l:%M:%S %p"
date_fmt -> "%A, %B %e, %r %Z %Y"
d_fmt -> "%d//%m//%Y"
am_pm -> [2]
	"WD"
	"WB"
mon -> [12]
	"Amajjii"
	"Guraandhala"
	"Bitooteessa"
	"Elba"
	"Caamsa"
	"Waxabajjii"
	"Adooleessa"
	"Hagayya"
	"Fuulbana"
	"Onkololeessa"
	"Sadaasa"
	"Muddee"
abmon -> [12]
	"Ama"
	"Gur"
	"Bit"
	"Elb"
	"Cam"
	"Wax"
	"Ado"
	"Hag"
	"Ful"
	"Onk"
	"Sad"
	"Mud"
d_t_fmt -> "%A, %B %e, %Y %r %Z"
day -> [7]
	"Dilbata"
	"Wiixata"
	"Qibxata"
	"Roobii"
	"Kamiisa"
	"Jimaata"
	"Sanbata"
t_fmt_ampm -> "%l:%M:%S %p"
abday -> [7]
	"Dil"
	"Wix"
	"Qib"
	"Rob"
	"Kam"
	"Jim"
	"San"

category: LC_MESSAGES
nostr -> "miti"
yesstr -> "eeyyee"
noexpr -> "^[-0mMnN]"
yesexpr -> "^[+1eEyY]"

category: LC_TELEPHONE
int_select -> "000"
int_prefix -> "254"
tel_int_fmt -> "%c-%a-%l"
tel_dom_fmt -> "%a-%l"

category: LC_ADDRESS
lang_lib -> "orm"
lang_ab -> "om"
lang_term -> "orm"
lang_name -> "Oromoo"
country_car -> "EAK"
country_num -> 404
country_ab3 -> "KEN"
country_post -> "KEN"
country_ab2 -> "KE"
country_name -> "Keeniyaa"
postal_fmt -> "%z%c%T%s%b%e%r"

category: LC_PAPER
width -> 210
height -> 297

category: LC_NAME
name_mrs -> "Ad"
name_miss -> "Du"
name_mr -> "Ob"
name_ms -> ""
name_gen -> ""
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> ","
decimal_point -> "."

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "KES "
int_frac_digits -> 2
currency_symbol -> "Ksh"
p_cs_precedes -> 1
p_sep_by_space -> 0
mon_decimal_point -> "."
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 0

category: LC_MEASUREMENT
measurement -> 1

category: LC_IDENTIFICATION
revision -> "0.20"
date -> "2003-07-05"
territory -> "Kenya"
fax -> ""
email -> "locales@geez.org"
tel -> ""
contact -> ""
language -> "Oromo"
address -> "7802 Solomon Seal Dr., Springfield, VA 22152, USA"
source -> "Ge'ez Frontier Foundation & Sagalee Oromoo Publishing Co. Inc."
title -> "Oromo language locale for Kenya."
