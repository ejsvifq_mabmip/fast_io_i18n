category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_select -> "00"
int_prefix -> "44"
tel_int_fmt -> "+%c %a %l"

category: LC_ADDRESS
lang_lib -> "cor"
lang_ab -> "kw"
lang_term -> "cor"
lang_name -> "kernewek"
country_car -> "GB"
country_num -> 826
country_ab3 -> "GBR"
country_ab2 -> "GB"
country_name -> "Rywvaneth Unys"
postal_fmt -> "%f%N%a%N%d%N%b%N%s %h %e %r%N%z %T%N%c%N"

category: LC_PAPER
width -> 210
height -> 297

category: LC_MESSAGES
nostr -> "na"
yesstr -> "ea"
noexpr -> "^[-0nN]"
yesexpr -> "^[+1eEyY]"

category: LC_TIME
week -> [3]
	7
	19971130
	4
am_pm -> [2]
	""
	""
t_fmt -> "%T"
first_weekday -> 2
d_fmt -> "%d//%m//%y"
date_fmt -> "%a %d %b %Y %T %Z"
mon -> [12]
	"mis Genver"
	"mis Hwevrer"
	"mis Meurth"
	"mis Ebrel"
	"mis Me"
	"mis Metheven"
	"mis Gortheren"
	"mis Est"
	"mis Gwynngala"
	"mis Hedra"
	"mis Du"
	"mis Kevardhu"
abmon -> [12]
	"Gen"
	"Hwe"
	"Meu"
	"Ebr"
	"Me"
	"Met"
	"Gor"
	"Est"
	"Gwn"
	"Hed"
	"Du"
	"Kev"
d_t_fmt -> "%a %d %b %Y %T"
day -> [7]
	"De Sul"
	"De Lun"
	"De Merth"
	"De Merher"
	"De Yow"
	"De Gwener"
	"De Sadorn"
t_fmt_ampm -> ""
abday -> [7]
	"Sul"
	"Lun"
	"Mth"
	"Mhr"
	"Yow"
	"Gwe"
	"Sad"

category: LC_NAME
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> ","
decimal_point -> "."

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "GBP "
int_frac_digits -> 2
currency_symbol -> "£"
p_cs_precedes -> 1
p_sep_by_space -> 0
mon_decimal_point -> "."
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 0

category: LC_IDENTIFICATION
revision -> "1.0"
date -> "2000-06-29"
territory -> "United Kingdom"
fax -> ""
email -> "bug-glibc-locales@gnu.org"
tel -> ""
contact -> ""
language -> "Cornish"
address -> "Croí Lár, Ballinahalla, Maigh Cuilinn,, Co. Gaillimh, Ireland"
source -> "Alastair McKinstry"
title -> "Cornish locale for Britain"
