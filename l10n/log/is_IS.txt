category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_select -> "00"
int_prefix -> "354"
tel_int_fmt -> "+%c %a %l"

category: LC_ADDRESS
lang_lib -> "ice"
lang_ab -> "is"
lang_term -> "isl"
lang_name -> "íslenska"
country_car -> "IS"
country_num -> 352
country_ab3 -> "ISL"
country_ab2 -> "IS"
country_name -> "Ísland"
postal_fmt -> "%f%N%a%N%d%N%b%N%s %h %e %r%N%z %T%N%c%N"

category: LC_PAPER
width -> 210
height -> 297

category: LC_MESSAGES
nostr -> "nei"
yesstr -> "já"
noexpr -> "^[-0nN]"
yesexpr -> "^[+1jJyY]"

category: LC_TIME
week -> [3]
	7
	19971130
	4
am_pm -> [2]
	"fh"
	"eh"
t_fmt -> "%T"
first_weekday -> 2
d_fmt -> "%a %e.%b %Y"
date_fmt -> "%a %e.%b %Y, %T %Z"
mon -> [12]
	"janúar"
	"febrúar"
	"mars"
	"apríl"
	"maí"
	"júní"
	"júlí"
	"ágúst"
	"september"
	"október"
	"nóvember"
	"desember"
abmon -> [12]
	"jan"
	"feb"
	"mar"
	"apr"
	"maí"
	"jún"
	"júl"
	"ágú"
	"sep"
	"okt"
	"nóv"
	"des"
d_t_fmt -> "%a %e.%b %Y, %T"
day -> [7]
	"sunnudagur"
	"mánudagur"
	"þriðjudagur"
	"miðvikudagur"
	"fimmtudagur"
	"föstudagur"
	"laugardagur"
t_fmt_ampm -> ""
abday -> [7]
	"sun"
	"mán"
	"þri"
	"mið"
	"fim"
	"fös"
	"lau"

category: LC_NAME
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> "."
decimal_point -> ","

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "ISK "
int_frac_digits -> 0
currency_symbol -> "kr"
p_cs_precedes -> 0
p_sep_by_space -> 1
mon_decimal_point -> ","
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> "."
n_cs_precedes -> 0
negative_sign -> "-"
frac_digits -> 0
n_sep_by_space -> 1

category: LC_IDENTIFICATION
revision -> "1.0"
date -> "2000-06-29"
territory -> "Iceland"
fax -> ""
email -> "bug-glibc-locales@gnu.org"
tel -> ""
contact -> ""
language -> "Icelandic"
address -> "Keldnaholt-ITI', IS-112 Reykjavi'k, Iceland"
source -> "Stadlarad I'slands"
title -> "Icelandic locale for Iceland"
