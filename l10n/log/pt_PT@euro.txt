category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_prefix -> "351"
int_select -> "00"
tel_dom_fmt -> "%l"
tel_int_fmt -> "+%c %l"

category: LC_ADDRESS
lang_lib -> "por"
lang_ab -> "pt"
lang_term -> "por"
lang_name -> "português"
country_car -> "P"
country_num -> 620
country_ab3 -> "PRT"
country_ab2 -> "PT"
country_name -> "Portugal"
postal_fmt -> "%f%N%a%N%d%N%b%N%s %h %e %r%N%z %T%N%c%N"

category: LC_PAPER
width -> 210
height -> 297

category: LC_TIME
week -> [3]
	7
	19971130
	4
am_pm -> [2]
	""
	""
t_fmt -> "%T"
first_weekday -> 2
d_fmt -> "%d//%m//%Y"
date_fmt -> "%a %d %b %Y %T %Z"
mon -> [12]
	"janeiro"
	"fevereiro"
	"março"
	"abril"
	"maio"
	"junho"
	"julho"
	"agosto"
	"setembro"
	"outubro"
	"novembro"
	"dezembro"
abmon -> [12]
	"jan"
	"fev"
	"mar"
	"abr"
	"mai"
	"jun"
	"jul"
	"ago"
	"set"
	"out"
	"nov"
	"dez"
d_t_fmt -> "%a %d %b %Y %T"
day -> [7]
	"domingo"
	"segunda"
	"terça"
	"quarta"
	"quinta"
	"sexta"
	"sábado"
t_fmt_ampm -> ""
abday -> [7]
	"dom"
	"seg"
	"ter"
	"qua"
	"qui"
	"sex"
	"sáb"

category: LC_NAME
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	0
	0
thousands_sep -> ""
decimal_point -> ","

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "EUR "
int_frac_digits -> 2
currency_symbol -> "€"
p_cs_precedes -> 0
p_sep_by_space -> 1
mon_decimal_point -> ","
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> "."
n_cs_precedes -> 0
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 1

category: LC_MESSAGES
nostr -> "não"
yesstr -> "sim"
noexpr -> "^[-0nN]"
yesexpr -> "^[+1SsyY]"

category: LC_IDENTIFICATION
revision -> "1.0"
date -> "2000-08-20"
territory -> "Portugal"
fax -> ""
email -> "bug-glibc-locales@gnu.org"
tel -> ""
contact -> ""
language -> "Portuguese"
address -> "https:////www.gnu.org//software//libc//"
source -> "Free Software Foundation, Inc."
title -> "Portuguese locale for Portugal with Euro"
