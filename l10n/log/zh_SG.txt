category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_select -> "001"
int_prefix -> "65"
tel_int_fmt -> "+%c ;%a ;%l"

category: LC_ADDRESS
lang_lib -> "chi"
lang_ab -> "zh"
lang_term -> "zho"
lang_name -> "简体中文"
country_car -> "SGP"
country_num -> 702
country_ab3 -> "SGP"
country_ab2 -> "SG"
country_name -> "新加坡"
postal_fmt -> "%z%c%T%s%b%e%r"

category: LC_PAPER
width -> 210
height -> 297

category: LC_MESSAGES
nostr -> "不是"
yesstr -> "是"
noexpr -> "^[-0nNｎＮ不否]"
yesexpr -> "^[+1yYｙＹ是]"

category: LC_TIME
week -> [3]
	7
	19971130
	1
t_fmt -> "%H时%M分%S秒 %Z"
d_fmt -> "%Y年%m月%d日"
date_fmt -> "%Y年%m月%d日 %H时%M分%S秒 %Z"
am_pm -> [2]
	"上午"
	"下午"
mon -> [12]
	"一月"
	"二月"
	"三月"
	"四月"
	"五月"
	"六月"
	"七月"
	"八月"
	"九月"
	"十月"
	"十一月"
	"十二月"
abmon -> [12]
	"一月"
	"二月"
	"三月"
	"四月"
	"五月"
	"六月"
	"七月"
	"八月"
	"九月"
	"十月"
	"十一月"
	"十二月"
d_t_fmt -> "%Y年%m月%d日 %H时%M分%S秒"
day -> [7]
	"星期日"
	"星期一"
	"星期二"
	"星期三"
	"星期四"
	"星期五"
	"星期六"
t_fmt_ampm -> ""
abday -> [7]
	"日"
	"一"
	"二"
	"三"
	"四"
	"五"
	"六"

category: LC_NAME
name_mrs -> "Mrs."
name_miss -> "Miss."
name_mr -> "Mr."
name_ms -> "Ms."
name_gen -> ""
name_fmt -> "%p%t%f%t%g"

category: LC_NUMERIC
grouping -> 3
thousands_sep -> ","
decimal_point -> "."

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "SGD "
int_frac_digits -> 2
currency_symbol -> "$"
p_cs_precedes -> 1
p_sep_by_space -> 0
mon_decimal_point -> "."
n_sign_posn -> 0
mon_grouping -> 3
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 0

category: LC_IDENTIFICATION
revision -> "1.0"
date -> "2000,October,27 (XML source:2000,July,20)"
territory -> "Singapore"
fax -> ""
email -> "bug-glibc-locales@gnu.org"
tel -> ""
contact -> ""
language -> "Chinese"
address -> "1623-14, Shimotsuruma, Yamato-shi, Kanagawa-ken, 242-8502, Japan"
source -> "IBM Globalization Center of Competency, Yamato Software Laboratory"
title -> "Chinese language locale for Singapore"
