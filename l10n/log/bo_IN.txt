category: LC_MEASUREMENT
measurement -> 1

category: LC_ADDRESS
lang_lib -> "tib"
lang_ab -> "bo"
lang_term -> "bod"
lang_name -> "བོད་སྐད་"
country_car -> "IND"
country_num -> 356
country_ab3 -> "IND"
country_ab2 -> "IN"
country_name -> "རྒྱ་གར་"
postal_fmt -> "%z%c%T%s%b%e%r"

category: LC_PAPER
width -> 210
height -> 297

category: LC_TELEPHONE
int_select -> "00"
int_prefix -> "91"
tel_int_fmt -> "+%c ;%a ;%l"

category: LC_MESSAGES
nostr -> "མིན།"
yesstr -> "ཡིན།"
noexpr -> "^[-0nNམ]"
yesexpr -> "^[+1yYཨ]"

category: LC_TIME
week -> [3]
	7
	19971130
	1
am_pm -> [2]
	"ངས་ཆ"
	"ཕྱི་ཆ"
t_fmt -> "ཆུ་ཚོད%Hཀསར་མ%Mཀསར་ཆ%S"
first_weekday -> 2
d_fmt -> "པསྱི་ལོ%yཟལ%mཚེས%d"
date_fmt -> "པསྱི་ལོ%yཟལ%mཚེས%dཆུ་ཚོད%Hཀསར་མ%Mཀསར་ཆ%S"
mon -> [12]
	"ཟླ་བ་དང་པ་"
	"ཟླ་བ་གཉིས་པ་"
	"ཟླ་བ་གསུམ་པ་"
	"ཟླ་བ་བཞི་པ་"
	"ཟླ་བ་ལྔ་ཕ་"
	"ཟླ་བ་དྲུག་པ་"
	"ཟླ་བ་བདུནཔ་"
	"ཟླ་བ་བརྒྱད་པ་"
	"ཟླ་བ་དགུ་པ་"
	"ཟླ་བ་བཅུ་པ་"
	"ཟླ་བ་བཅུ་གཅིག་པ་"
	"ཟླ་བ་བཅུ་གཉིས་པ་"
abmon -> [12]
	"ཟླ་༡"
	"ཟླ་༢"
	"ཟླ་༣"
	"ཟླ་༤"
	"ཟླ་༥"
	"ཟླ་༦"
	"ཟླ་༧"
	"ཟླ་༨"
	"ཟླ་༩"
	"ཟླ་༡༠"
	"ཟླ་༡༡"
	"ཟླ་༡༢"
d_t_fmt -> "པསྱི་ལོ%yཟལ%mཚེས%dཆུ་ཚོད%Hཀསར་མ%Mཀསར་ཆ%S"
day -> [7]
	"གཟའ་ཉི་མ་"
	"གཟའ་ཟླ་བ་"
	"གཟའ་མིག་དམར་"
	"གཟའ་ལྷག་ཕ་"
	"གཟའ་པུར་བུ་"
	"གཟའ་པ་སངས་"
	"གཟའ་སྤེན་ཕ་"
t_fmt_ampm -> "ཆུ་ཚོད%Iཀསར་མ%Mཀསར་ཆ%S %p"
abday -> [7]
	"ཉི་"
	"ཟླ་"
	"མིར་"
	"ལྷག་"
	"པུར་"
	"སངས་"
	"སྤེན་"

category: LC_NAME
name_fmt -> " "

category: LC_NUMERIC
grouping -> 3
thousands_sep -> ","
decimal_point -> "."

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "INR "
int_frac_digits -> 2
currency_symbol -> "₹"
p_cs_precedes -> 1
p_sep_by_space -> 0
mon_decimal_point -> "."
n_sign_posn -> 1
mon_grouping -> [2]
	3
	2
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 0

category: LC_IDENTIFICATION
revision -> "0.1"
date -> "2007-11-06"
territory -> "India"
fax -> ""
email -> "bug-glibc@gnu.org"
tel -> ""
contact -> ""
language -> "Tibetan"
address -> ""
source -> ""
title -> "Tibetan language locale for India"
