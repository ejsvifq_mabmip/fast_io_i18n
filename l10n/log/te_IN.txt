category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_select -> "00"
int_prefix -> "91"
tel_int_fmt -> "+%c ;%a ;%l"

category: LC_ADDRESS
lang_lib -> "tel"
lang_ab -> "te"
lang_term -> "tel"
lang_name -> "తెలుగు"
country_car -> "IND"
country_num -> 356
country_ab3 -> "IND"
country_ab2 -> "IN"
country_name -> "భారతదేశం"
postal_fmt -> "%a%N%d%N%f%N%r%t%e%N%h%t%b%N%s%t%N%T%t%z%N%S%N%c"

category: LC_PAPER
width -> 210
height -> 297

category: LC_MESSAGES
nostr -> "వద్దు"
yesstr -> "అవును"
noexpr -> "^[-0nNవ]"
yesexpr -> "^[+1yYఅ]"

category: LC_TIME
week -> [3]
	7
	19971130
	1
t_fmt -> "%p%I.%M.%S %Z"
d_fmt -> "%d-%m-%y"
date_fmt -> "%B %d %A %Y %p%I.%M.%S %Z"
am_pm -> [2]
	"ఉ."
	"సా."
mon -> [12]
	"జనవరి"
	"ఫిబ్రవరి"
	"మార్చి"
	"ఏప్రిల్"
	"మే"
	"జూన్"
	"జులై"
	"ఆగస్టు"
	"సెప్టెంబర్"
	"అక్టోబర్"
	"నవంబర్"
	"డిసెంబర్"
abmon -> [12]
	"జన"
	"ఫిబ్ర"
	"మార్చి"
	"ఏప్రి"
	"మే"
	"జూన్"
	"జులై"
	"ఆగ"
	"సెప్టెం"
	"అక్టో"
	"నవం"
	"డిసెం"
d_t_fmt -> "%B %d %A %Y %p%I.%M.%S"
day -> [7]
	"ఆదివారం"
	"సోమవారం"
	"మంగళవారం"
	"బుధవారం"
	"గురువారం"
	"శుక్రవారం"
	"శనివారం"
t_fmt_ampm -> "%p%I.%M.%S %Z"
abday -> [7]
	"ఆది"
	"సోమ"
	"మంగళ"
	"బుధ"
	"గురు"
	"శుక్ర"
	"శని"

category: LC_NAME
name_mrs -> "శ్రీమతి"
name_miss -> "కుమారి"
name_mr -> "శ్రీ"
name_ms -> ""
name_gen -> ""
name_fmt -> "%p%t%f%t%g%t%m"

category: LC_NUMERIC
grouping -> [2]
	3
	2
thousands_sep -> ","
decimal_point -> "."

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "INR "
int_frac_digits -> 2
currency_symbol -> "₹"
p_cs_precedes -> 1
p_sep_by_space -> 0
mon_decimal_point -> "."
n_sign_posn -> 1
mon_grouping -> [2]
	3
	2
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 0

category: LC_IDENTIFICATION
revision -> "0.95"
date -> "2004-10-05"
territory -> "India"
fax -> ""
email -> "bug-glibc-locales@gnu.org"
tel -> ""
contact -> ""
language -> "Telugu"
address -> "1623-14, Shimotsuruma, Yamato-shi, Kanagawa-ken, 242-8502, Japan"
source -> "IBM Globalization Center of Competency, Yamato Software Laboratory"
title -> "Telugu language locale for India"
