category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_select -> "00"
int_prefix -> "675"
tel_int_fmt -> "+%c %a %l"

category: LC_ADDRESS
lang_lib -> "yuw"
lang_ab -> ""
lang_term -> "yuw"
country_car -> "PNG"
lang_name -> "Uruwa"
country_num -> 598
country_ab3 -> "PNG"
country_post -> ""
country_ab2 -> "PG"
country_name -> "Papua New Guinea"
postal_fmt -> "%f%N%a%N%d%N%b%N%s %h %e %r%N%z %T%N%c%N"

category: LC_PAPER
width -> 210
height -> 297

category: LC_MESSAGES
nostr -> "muuno"
yesstr -> "öö"
noexpr -> "^[-0nNmM]"
yesexpr -> "^[+1yYöÖ]"

category: LC_TIME
week -> [3]
	7
	19971130
	1
am_pm -> [2]
	"AM"
	"PM"
t_fmt -> "%T"
d_fmt -> "%d//%m//%y"
date_fmt -> "%a %d %b %Y %T %Z"
mon -> [12]
	"jenuari"
	"febuari"
	"mas"
	"epril"
	"mei"
	"jun"
	"julai"
	"ögus"
	"septemba"
	"öktoba"
	"nöwemba"
	"diksemba"
abmon -> [12]
	"jen"
	"feb"
	"mas"
	"epr"
	"mei"
	"jun"
	"jul"
	"ögu"
	"sep"
	"ökt"
	"nöw"
	"dis"
d_t_fmt -> "%a %d %b %Y %T"
day -> [7]
	"sönda"
	"mönda"
	"sinda"
	"mitiwö"
	"sogipbono"
	"nenggo"
	"söndanggie"
t_fmt_ampm -> "%I:%M:%S %p"
abday -> [7]
	"sön"
	"mön"
	"sin"
	"mit"
	"soi"
	"nen"
	"sab"

category: LC_NAME
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> ","
decimal_point -> "."

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "PGK "
int_frac_digits -> 2
currency_symbol -> "K"
p_cs_precedes -> 1
p_sep_by_space -> 0
mon_decimal_point -> "."
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 0

category: LC_IDENTIFICATION
revision -> "1.0"
date -> "2016-12-07"
territory -> "Papua New Guinea"
fax -> ""
email -> "nungon.localization@gmail.com"
tel -> ""
contact -> "Hannah Sarvasy"
language -> "Yau"
address -> ""
source -> "Information from native speakers"
title -> "Yau/Nungon locale for Papua New Guinea"
