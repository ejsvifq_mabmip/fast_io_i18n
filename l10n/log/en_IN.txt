category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_select -> "00"
int_prefix -> "91"
tel_int_fmt -> "+%c ;%a ;%l"

category: LC_ADDRESS
lang_lib -> "eng"
lang_ab -> "en"
lang_term -> "eng"
lang_name -> "English"
country_car -> "IND"
country_num -> 356
country_ab3 -> "IND"
country_ab2 -> "IN"
country_name -> "India"
postal_fmt -> "%z%c%T%s%b%e%r"

category: LC_PAPER
width -> 210
height -> 297

category: LC_MESSAGES
nostr -> "no"
yesstr -> "yes"
noexpr -> "^[-0nN]"
yesexpr -> "^[+1yY]"

category: LC_TIME
week -> [3]
	7
	19971130
	1
t_fmt -> "%I:%M:%S %p %Z"
d_fmt -> "%d//%m//%y"
date_fmt -> "%A %d %B %Y %I:%M:%S %p %Z"
am_pm -> [2]
	"AM"
	"PM"
mon -> [12]
	"January"
	"February"
	"March"
	"April"
	"May"
	"June"
	"July"
	"August"
	"September"
	"October"
	"November"
	"December"
abmon -> [12]
	"Jan"
	"Feb"
	"Mar"
	"Apr"
	"May"
	"Jun"
	"Jul"
	"Aug"
	"Sep"
	"Oct"
	"Nov"
	"Dec"
d_t_fmt -> "%A %d %B %Y %I:%M:%S %p"
day -> [7]
	"Sunday"
	"Monday"
	"Tuesday"
	"Wednesday"
	"Thursday"
	"Friday"
	"Saturday"
t_fmt_ampm -> "%I:%M:%S %p %Z"
abday -> [7]
	"Sun"
	"Mon"
	"Tue"
	"Wed"
	"Thu"
	"Fri"
	"Sat"

category: LC_NAME
name_mrs -> "Mrs."
name_miss -> "Ms."
name_mr -> "Mr."
name_ms -> ""
name_gen -> ""
name_fmt -> "%p%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	2
thousands_sep -> ","
decimal_point -> "."

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "INR "
int_frac_digits -> 2
currency_symbol -> "₹"
p_cs_precedes -> 1
p_sep_by_space -> 0
mon_decimal_point -> "."
n_sign_posn -> 1
mon_grouping -> [2]
	3
	2
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 0

category: LC_IDENTIFICATION
revision -> "1.0"
date -> "2000,October,27 (XML source:2000,July,20)"
territory -> "India"
fax -> ""
email -> "bug-glibc-locales@gnu.org"
tel -> ""
contact -> ""
language -> "English"
address -> "1623-14, Shimotsuruma, Yamato-shi, Kanagawa-ken, 242-8502, Japan"
source -> "IBM Globalization Center of Competency, Yamato Software Laboratory"
title -> "English language locale for India"
