category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_select -> "00"
int_prefix -> "32"
tel_int_fmt -> "+%c %a %l"

category: LC_ADDRESS
lang_lib -> "dut"
lang_ab -> "nl"
lang_term -> "nld"
country_car -> "B"
lang_name -> "Nederlands"
country_num -> 056
country_ab3 -> "BEL"
country_ab2 -> "BE"
country_name -> "België"
postal_fmt -> "%f%N%a%N%d%N%b%N%s %h %e %r%N%z %T%N%c%N"

category: LC_PAPER
width -> 210
height -> 297

category: LC_TIME
week -> [3]
	7
	19971130
	4
am_pm -> [2]
	""
	""
t_fmt -> "%T"
first_weekday -> 2
d_fmt -> "%d-%m-%y"
date_fmt -> "%a %d %b %Y %T %Z"
mon -> [12]
	"januari"
	"februari"
	"maart"
	"april"
	"mei"
	"juni"
	"juli"
	"augustus"
	"september"
	"oktober"
	"november"
	"december"
abmon -> [12]
	"jan"
	"feb"
	"mrt"
	"apr"
	"mei"
	"jun"
	"jul"
	"aug"
	"sep"
	"okt"
	"nov"
	"dec"
d_t_fmt -> "%a %d %b %Y %T"
day -> [7]
	"zondag"
	"maandag"
	"dinsdag"
	"woensdag"
	"donderdag"
	"vrijdag"
	"zaterdag"
t_fmt_ampm -> ""
abday -> [7]
	"zo"
	"ma"
	"di"
	"wo"
	"do"
	"vr"
	"za"

category: LC_NAME
name_mrs -> "mevrouw"
name_miss -> "mevrouw"
name_mr -> "heer"
name_ms -> "mevrouw"
name_gen -> "heer of mevrouw"
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> "."
decimal_point -> ","

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "EUR "
int_frac_digits -> 2
currency_symbol -> "€"
p_cs_precedes -> 0
p_sep_by_space -> 1
mon_decimal_point -> ","
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> "."
n_cs_precedes -> 0
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 1

category: LC_MESSAGES
nostr -> "nee"
yesstr -> "ja"
noexpr -> "^[-0nN]"
yesexpr -> "^[+1jJyY]"

category: LC_IDENTIFICATION
revision -> "1.0"
date -> "2000-08-21"
territory -> "Belgium"
fax -> ""
email -> "bug-glibc-locales@gnu.org"
tel -> ""
contact -> ""
language -> "Dutch"
address -> "https:////www.gnu.org//software//libc//"
source -> "Free Software Foundation, Inc."
title -> "Dutch locale for Belgium with Euro"
