category: LC_TELEPHONE
int_select -> "8 10"
int_prefix -> "993"
tel_int_fmt -> "+%c %a %l"

category: LC_ADDRESS
lang_ab -> "tk"
lang_lib -> "tuk"
country_num -> 795
lang_term -> "tuk"
lang_name -> "türkmen dili"
country_car -> "TM"
country_ab3 -> "TKM"
country_post -> "TM"
country_ab2 -> "TM"
country_name -> "Türkmenistan"
postal_fmt -> "%f%N%a%N%d%N%b%N%s %h %e %r%N%z %T%N%c%N"

category: LC_PAPER
width -> 210
height -> 297

category: LC_MESSAGES
nostr -> "ýok"
yesstr -> "hawa"
noexpr -> "^[-0nNýÝ]"
yesexpr -> "^[+1hH]"

category: LC_MEASUREMENT
measurement -> 1

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "TMM "
int_frac_digits -> 2
currency_symbol -> "MANAT"
p_cs_precedes -> 0
p_sep_by_space -> 1
mon_decimal_point -> "."
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 0
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 1

category: LC_NAME
name_mrs -> ""
name_miss -> ""
name_mr -> ""
name_ms -> ""
name_gen -> ""
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> ","
decimal_point -> "."

category: LC_TIME
week -> [3]
	7
	19971130
	1
am_pm -> [2]
	""
	""
t_fmt -> "%T"
first_weekday -> 2
d_fmt -> "%d.%m.%Y"
date_fmt -> "%d.%m.%Y %T %Z"
abmon -> [12]
	"Ýan"
	"Few"
	"Mar"
	"Apr"
	"Maý"
	"Iýn"
	"Iýl"
	"Awg"
	"Sen"
	"Okt"
	"Noý"
	"Dek"
mon -> [12]
	"Ýanwar"
	"Fewral"
	"Mart"
	"Aprel"
	"Maý"
	"Iýun"
	"Iýul"
	"Awgust"
	"Sentýabr"
	"Oktýabr"
	"Noýabr"
	"Dekabr"
t_fmt_ampm -> ""
abday -> [7]
	"Duş"
	"Siş"
	"Çar"
	"Pen"
	"Ann"
	"Şen"
	"Ýek"
d_t_fmt -> "%d.%m.%Y %T"
day -> [7]
	"Duşenbe"
	"Sişenbe"
	"Çarşenbe"
	"Penşenbe"
	"Anna"
	"Şenbe"
	"Ýekşenbe"

category: LC_IDENTIFICATION
revision -> "0.5"
date -> "2011-04-09"
territory -> "Turkmenistan"
fax -> ""
email -> "pablo@walon.org & gmt314@yahoo.com"
tel -> ""
contact -> "Pablo Saratxaga & Ghorban M. Tavakoly"
language -> "Turkmen"
address -> ""
source -> "Ghorban M. Tavakoly"
title -> "Turkmen locale for Turkmenistan"
