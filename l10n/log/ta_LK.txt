category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_prefix -> "94"
int_select -> "00"
tel_dom_fmt -> "(%a) %l"
tel_int_fmt -> "+%c (%a) %l"

category: LC_ADDRESS
lang_lib -> "tam"
lang_ab -> "ta"
lang_term -> "tam"
lang_name -> "தமிழ்"
country_car -> "CL"
country_num -> 144
country_ab3 -> "LKA"
country_ab2 -> "LK"
country_name -> "இலங்கை"
postal_fmt -> "%z%c%T%s%b%e%r"

category: LC_PAPER
width -> 210
height -> 297

category: LC_TIME
week -> [3]
	7
	19971130
	1
t_fmt -> "%H:%M:%S %Z"
first_weekday -> 2
d_fmt -> "%-d//%-m//%y"
date_fmt -> "%A %d %B %Y %H:%M:%S %Z"
am_pm -> [2]
	"காலை"
	"மாலை"
mon -> [12]
	"ஜனவரி"
	"பிப்ரவரி"
	"மார்ச்"
	"ஏப்ரல்"
	"மே"
	"ஜூன்"
	"ஜூலை"
	"ஆகஸ்ட்"
	"செப்டம்பர்"
	"அக்டோபர்"
	"நவம்பர்"
	"டிசம்பர்"
abmon -> [12]
	"ஜன"
	"பிப்"
	"மார்"
	"ஏப்"
	"மே"
	"ஜூன்"
	"ஜூலை"
	"ஆக"
	"செப்"
	"அக்"
	"நவ"
	"டிச"
d_t_fmt -> "%A %d %B %Y %H:%M:%S"
day -> [7]
	"ஞாயிறு"
	"திங்கள்"
	"செவ்வாய்"
	"புதன்"
	"வியாழன்"
	"வெள்ளி"
	"சனி"
t_fmt_ampm -> ""
abday -> [7]
	"ஞா"
	"தி"
	"செ"
	"பு"
	"வி"
	"வெ"
	"ச"

category: LC_NAME
name_mrs -> "திருமதி"
name_miss -> "செல்வி"
name_mr -> "திரு"
name_ms -> "Ms."
name_gen -> ""
name_fmt -> "%p%t%f%t%g"

category: LC_NUMERIC
grouping -> [2]
	3
	2
thousands_sep -> ","
decimal_point -> "."

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "LKR "
int_frac_digits -> 2
currency_symbol -> "රු"
p_cs_precedes -> 1
p_sep_by_space -> 1
mon_decimal_point -> "."
n_sign_posn -> 1
mon_grouping -> 3
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 1

category: LC_MESSAGES
nostr -> "இல்லை"
yesstr -> "ஆம்"
noexpr -> "^[-0nNஇ]"
yesexpr -> "^[+1yYஆ]"

category: LC_IDENTIFICATION
revision -> "1.0"
date -> "2011,August,11"
territory -> "Sri Lanka"
fax -> ""
email -> "yogaraj.ubuntu@gmail.com"
tel -> ""
contact -> "94-777-315206"
language -> "Tamil"
address -> "30//36Q -2//1,Charles Apartments, De Silva Cross Rd, Kalubowila, Dehiwela, SriLanka."
source -> "J.Yogaraj"
title -> "Tamil language locale for Sri Lanka"
