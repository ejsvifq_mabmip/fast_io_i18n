category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_select -> "011"
int_prefix -> "1"
tel_int_fmt -> "+%c %a %l"

category: LC_ADDRESS
lang_lib -> "spa"
lang_ab -> "es"
lang_term -> "spa"
lang_name -> "español"
country_car -> "DOM"
country_num -> 214
country_ab3 -> "DOM"
country_ab2 -> "DO"
country_name -> "República Dominicana"
postal_fmt -> "%f%N%a%N%d%N%b%N%s %h %e %r%N%z %T%N%c%N"

category: LC_PAPER
width -> 210
height -> 297

category: LC_TIME
week -> [3]
	7
	19971130
	1
am_pm -> [2]
	""
	""
t_fmt -> "%T"
d_fmt -> "%d//%m//%y"
date_fmt -> "%a %d %b %Y %T %Z"
mon -> [12]
	"enero"
	"febrero"
	"marzo"
	"abril"
	"mayo"
	"junio"
	"julio"
	"agosto"
	"septiembre"
	"octubre"
	"noviembre"
	"diciembre"
abmon -> [12]
	"ene"
	"feb"
	"mar"
	"abr"
	"may"
	"jun"
	"jul"
	"ago"
	"sep"
	"oct"
	"nov"
	"dic"
d_t_fmt -> "%a %d %b %Y %T"
day -> [7]
	"domingo"
	"lunes"
	"martes"
	"miércoles"
	"jueves"
	"viernes"
	"sábado"
t_fmt_ampm -> ""
abday -> [7]
	"dom"
	"lun"
	"mar"
	"mié"
	"jue"
	"vie"
	"sáb"

category: LC_NAME
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> ","
decimal_point -> "."

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "DOP "
int_frac_digits -> 2
currency_symbol -> "RD$"
p_cs_precedes -> 1
p_sep_by_space -> 1
mon_decimal_point -> "."
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 1

category: LC_MESSAGES
nostr -> "no"
yesstr -> "sí"
noexpr -> "^[-0nN]"
yesexpr -> "^[+1sSyY]"

category: LC_IDENTIFICATION
revision -> "1.0"
date -> "2000-06-29"
territory -> "Dominican Republic"
fax -> ""
email -> "bug-glibc-locales@gnu.org"
tel -> ""
contact -> ""
language -> "Spanish"
address -> "Sankt Jørgens Alle 8, DK-1615 København V, Danmark"
source -> "RAP"
title -> "Spanish locale for Dominican Republic"
