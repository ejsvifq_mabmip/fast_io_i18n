category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_select -> "00"
int_prefix -> "352"
tel_dom_fmt -> "%A %l"
tel_int_fmt -> "+%c %a %l"

category: LC_ADDRESS
lang_lib -> "ltz"
lang_ab -> "lb"
country_isbn -> 2
lang_term -> "ltz"
lang_name -> "Lëtzebuergesch"
country_car -> "L"
country_num -> 442
country_ab3 -> "LUX"
country_post -> "L"
country_ab2 -> "LU"
country_name -> "Lëtzebuerg"
postal_fmt -> "%f%N%a%N%d%N%b%N%s %h %e %r%N%z %T%N%c%N"

category: LC_PAPER
width -> 210
height -> 297

category: LC_MESSAGES
nostr -> "nee"
yesstr -> "jo"
noexpr -> "^[-0nN]"
yesexpr -> "^[+1jJyY]"

category: LC_TIME
week -> [3]
	7
	19971130
	4
t_fmt -> "%T"
first_weekday -> 2
date_fmt -> "%a %-d. %b %H:%M:%S %Z %Y"
d_fmt -> "%d.%m.%Y"
am_pm -> [2]
	""
	""
mon -> [12]
	"Januar"
	"Februar"
	"Mäerz"
	"Abrëll"
	"Mee"
	"Juni"
	"Juli"
	"August"
	"September"
	"Oktober"
	"November"
	"Dezember"
abmon -> [12]
	"Jan"
	"Feb"
	"Mäe"
	"Abr"
	"Mee"
	"Jun"
	"Jul"
	"Aug"
	"Sep"
	"Okt"
	"Nov"
	"Dez"
d_t_fmt -> "%a %d. %b %Y %T"
day -> [7]
	"Sonndeg"
	"Méindeg"
	"Dënschdeg"
	"Mëttwoch"
	"Donneschdeg"
	"Freideg"
	"Samschdeg"
t_fmt_ampm -> ""
abday -> [7]
	"So"
	"Mé"
	"Dë"
	"Më"
	"Do"
	"Fr"
	"Sa"

category: LC_NAME
name_ms -> "Madame"
name_mrs -> "Madame"
name_mr -> "Här"
name_miss -> "Joffer"
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> "."
decimal_point -> ","

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "EUR "
int_frac_digits -> 2
currency_symbol -> "€"
p_cs_precedes -> 0
p_sep_by_space -> 1
mon_decimal_point -> ","
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> "."
n_cs_precedes -> 0
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 1

category: LC_IDENTIFICATION
revision -> "0.2"
date -> "2011-01-28"
territory -> "Luxembourg"
fax -> ""
email -> "pit@wenkin.lu"
tel -> ""
contact -> "Pit Wenkin"
language -> "Luxembourgish"
address -> ""
source -> ""
title -> "Luxembourgish locale for Luxembourg"
