category: LC_TIME
week -> [3]
	7
	19971130
	1
t_fmt -> "%T"
date_fmt -> "%A, %B %e, %X %Z %Y"
d_fmt -> "%d//%m//%Y"
am_pm -> [2]
	"subaxnimo"
	"galabnimo"
mon -> [12]
	"Bisha Koobaad"
	"Bisha Labaad"
	"Bisha Saddexaad"
	"Bisha Afraad"
	"Bisha Shanaad"
	"Bisha Lixaad"
	"Bisha Todobaad"
	"Bisha Sideedaad"
	"Bisha Sagaalaad"
	"Bisha Tobnaad"
	"Bisha Kow iyo Tobnaad"
	"Bisha Laba iyo Tobnaad"
abmon -> [12]
	"Kob"
	"Lab"
	"Sad"
	"Afr"
	"Sha"
	"Lix"
	"Tod"
	"Sid"
	"Sag"
	"Tob"
	"KIT"
	"LIT"
d_t_fmt -> "%A, %B %e, %Y %X %Z"
day -> [7]
	"Axad"
	"Isniin"
	"Salaaso"
	"Arbaco"
	"Khamiis"
	"Jimco"
	"Sabti"
t_fmt_ampm -> ""
abday -> [7]
	"Axd"
	"Isn"
	"Sal"
	"Arb"
	"Kha"
	"Jim"
	"Sab"

category: LC_MESSAGES
nostr -> "maya"
yesstr -> "haa"
noexpr -> "^[-0nN]"
yesexpr -> "^[+1yY]"

category: LC_TELEPHONE
int_select -> "000"
int_prefix -> "254"
tel_int_fmt -> "%c-%a-%l"
tel_dom_fmt -> "%a-%l"

category: LC_ADDRESS
lang_lib -> "som"
lang_ab -> "so"
lang_term -> "som"
lang_name -> "Soomaali"
country_car -> "EAK"
country_num -> 404
country_ab3 -> "KEN"
country_post -> "KEN"
country_ab2 -> "KE"
country_name -> "Kiiniya"
postal_fmt -> "%z%c%T%s%b%e%r"

category: LC_PAPER
width -> 210
height -> 297

category: LC_NAME
name_mrs -> "Mw"
name_miss -> ""
name_mr -> "Md"
name_ms -> "Mw"
name_gen -> ""
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> ","
decimal_point -> "."

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "KES "
int_frac_digits -> 2
currency_symbol -> "Ksh"
p_cs_precedes -> 1
p_sep_by_space -> 0
mon_decimal_point -> "."
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 0

category: LC_MEASUREMENT
measurement -> 1

category: LC_IDENTIFICATION
revision -> "0.20"
date -> "2003-07-05"
territory -> "Kenya"
fax -> ""
email -> "locales@geez.org"
tel -> ""
contact -> ""
language -> "Somali"
address -> "7802 Solomon Seal Dr., Springfield, VA 22152, USA"
source -> "Ge'ez Frontier Foundation"
title -> "Somali language locale for Kenya"
