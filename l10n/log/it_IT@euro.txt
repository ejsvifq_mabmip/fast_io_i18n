category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_select -> "00"
int_prefix -> "39"
tel_int_fmt -> "+%c %a %l"

category: LC_ADDRESS
lang_lib -> "ita"
lang_ab -> "it"
lang_term -> "ita"
lang_name -> "italiano"
country_car -> "I"
country_isbn -> "978-88,979-12"
country_num -> 380
country_ab3 -> "ITA"
country_ab2 -> "IT"
country_name -> "Italia"
postal_fmt -> "%f%N%a%N%d%N%b%N%s %h %e %r%N%z %T%N%c%N"

category: LC_PAPER
width -> 210
height -> 297

category: LC_TIME
week -> [3]
	7
	19971130
	4
am_pm -> [2]
	""
	""
t_fmt -> "%T"
first_weekday -> 2
date_fmt -> "%a %-d %b %Y, %T, %Z"
d_fmt -> "%d//%m//%Y"
mon -> [12]
	"gennaio"
	"febbraio"
	"marzo"
	"aprile"
	"maggio"
	"giugno"
	"luglio"
	"agosto"
	"settembre"
	"ottobre"
	"novembre"
	"dicembre"
abmon -> [12]
	"gen"
	"feb"
	"mar"
	"apr"
	"mag"
	"giu"
	"lug"
	"ago"
	"set"
	"ott"
	"nov"
	"dic"
d_t_fmt -> "%a %-d %b %Y, %T"
day -> [7]
	"domenica"
	"lunedì"
	"martedì"
	"mercoledì"
	"giovedì"
	"venerdì"
	"sabato"
t_fmt_ampm -> ""
abday -> [7]
	"dom"
	"lun"
	"mar"
	"mer"
	"gio"
	"ven"
	"sab"

category: LC_NAME
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> "."
decimal_point -> ","

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "EUR "
int_frac_digits -> 2
currency_symbol -> "€"
p_cs_precedes -> 1
p_sep_by_space -> 1
mon_decimal_point -> ","
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> "."
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 1

category: LC_MESSAGES
nostr -> "no"
yesstr -> "sì"
noexpr -> "^[-0nN]"
yesexpr -> "^[+1sSyY]"

category: LC_IDENTIFICATION
revision -> "1.0"
date -> "2000-08-20"
territory -> "Italy"
fax -> ""
email -> "bug-glibc-locales@gnu.org"
tel -> ""
contact -> ""
language -> "Italian"
address -> "https:////www.gnu.org//software//libc//"
source -> "Free Software Foundation, Inc."
title -> "Italian locale for Italy with Euro"
