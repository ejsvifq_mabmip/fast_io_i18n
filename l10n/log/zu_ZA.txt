category: LC_MEASUREMENT
measurement -> 1

category: LC_TELEPHONE
int_prefix -> "27"
int_select -> "00"
tel_dom_fmt -> "(%A) %l"
tel_int_fmt -> "+%c %a %l"

category: LC_ADDRESS
country_num -> 710
postal_fmt -> "%f%N%a%N%d%N%b%N%s %h %e %r%N%z %T%N%c%N"
lang_lib -> "zul"
lang_ab -> "zu"
lang_term -> "zul"
country_car -> "ZA"
lang_name -> "isiZulu"
country_ab3 -> "ZAF"
country_post -> "ZA"
country_ab2 -> "ZA"
country_name -> "iNingizimu Afrika"

category: LC_PAPER
width -> 210
height -> 297

category: LC_MESSAGES
nostr -> "cha"
yesstr -> "yebo"
noexpr -> "^[-0nNcC]"
yesexpr -> "^[+1yY]"

category: LC_TIME
week -> [3]
	7
	19971130
	1
am_pm -> [2]
	""
	""
t_fmt -> "%T"
d_fmt -> "%d//%m//%Y"
date_fmt -> "%a %d %b %Y %T %Z"
mon -> [12]
	"Januwari"
	"Februwari"
	"Mashi"
	"Ephreli"
	"Meyi"
	"Juni"
	"Julayi"
	"Agasti"
	"Septhemba"
	"Okthoba"
	"Novemba"
	"Disemba"
abmon -> [12]
	"Jan"
	"Feb"
	"Mas"
	"Eph"
	"Mey"
	"Jun"
	"Jul"
	"Aga"
	"Sep"
	"Okt"
	"Nov"
	"Dis"
d_t_fmt -> "%a %d %b %Y %T"
day -> [7]
	"iSonto"
	"uMsombuluko"
	"uLwesibili"
	"uLwesithathu"
	"uLwesine"
	"uLwesihlanu"
	"uMgqibelo"
t_fmt_ampm -> ""
abday -> [7]
	"Son"
	"Mso"
	"Bil"
	"Tha"
	"Sin"
	"Hla"
	"Mgq"

category: LC_NAME
name_ms -> ""
name_mrs -> "Nkosikazi"
name_mr -> "Mnumzane"
name_miss -> "Nkosazane"
name_fmt -> "%d%t%g%t%m%t%f"

category: LC_NUMERIC
grouping -> [2]
	3
	3
thousands_sep -> ","
decimal_point -> "."

category: LC_MONETARY
p_sign_posn -> 1
int_curr_symbol -> "ZAR "
int_frac_digits -> 2
currency_symbol -> "R"
p_cs_precedes -> 1
p_sep_by_space -> 0
mon_decimal_point -> "."
n_sign_posn -> 1
mon_grouping -> [2]
	3
	3
positive_sign -> ""
mon_thousands_sep -> ","
n_cs_precedes -> 1
negative_sign -> "-"
frac_digits -> 2
n_sep_by_space -> 0

category: LC_IDENTIFICATION
revision -> "0.3"
date -> "2005-10-13"
territory -> "South Africa"
fax -> ""
email -> "dwayne@translate.org.za"
tel -> ""
contact -> "Dwayne Bailey"
language -> "Zulu"
address -> "Box 28364, Sunnyside, 0132, South Africa"
source -> "Zuza Software Foundation (Translate.org.za)"
title -> "Zulu locale for South Africa"
