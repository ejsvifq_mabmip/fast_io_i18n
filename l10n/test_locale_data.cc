#include"parsing.h"

int main(int argc,char** argv)
{
	if(argc<2)
	{
		perr("Usage:",fast_io::mnp::chvw(*argv)," <filename>");
		return 1;
	}
	fast_io::dir_file df("localedata");

	std::string current_file(argv[1]);
	std::unordered_map<std::string,std::unordered_map<std::string,std::unordered_map<std::string,std::string>>> cache;
	fast_io::dir_file logdf("log");
	auto const& ref{fast_io_i18n::parsing_file(df,current_file,cache)};
	fast_io::obuf_file obf(at(logdf),current_file+".txt");
	fast_io_i18n::log_data(ref,obf);

	fast_io::dir_file resdf("result");
	fast_io::obuf_file resultobf(at(resdf),current_file+".cc");
	fast_io_i18n::output_result(ref,resultobf);
}
