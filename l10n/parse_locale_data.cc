#include"parsing.h"

int main(int argc,char** argv)
{
	std::string_view exec_charset_name("UTF-8");
	if(1<argc)
	{
		exec_charset_name=argv[1];
	}
	println("Generated execution charset is:",exec_charset_name);
	fast_io::dir_file df("localedata");
	fast_io::dir_file resdf("result");
	std::unordered_map<std::string,std::unordered_map<std::string,std::unordered_map<std::string,std::string>>> cache;
	fast_io::obuf_file cmake_file1(fast_io::concat("cmake_00_add_lib_",exec_charset_name,".txt"));
	fast_io::obuf_file cmake_file2(fast_io::concat("cmake_01_GCC_targets_",exec_charset_name,".txt"));
	fast_io::obuf_file cmake_file3(fast_io::concat("cmake_02_MSVC_targets_",exec_charset_name,".txt"));
	fast_io::obuf_file cmake_file4(fast_io::concat("cmake_03_installs_",exec_charset_name,".txt"));
	for(auto const& dirent: current(at(df)))
	{
		std::string fnm;
		fast_io::ostring_ref ref{std::addressof(fnm)};
		print(ref,dirent);
		using namespace std::string_view_literals;
		if(fnm=="."sv||fnm==".."sv)
			continue;
		if(fnm=="cns11643_stroke"||fnm=="i18n_ctype")
			continue;
		if(fnm.starts_with("iso14651")||fnm.starts_with("translit"))
			continue;
		auto& result{fast_io_i18n::parsing_file(df,fnm,cache)};
		fast_io::obuf_file obf(at(resdf),fast_io::concat(fnm,".cc"));
		fast_io_i18n::output_result(result,obf);
		std::string result_filename;
		bool cmake_reserved{};
		for(auto& e : fnm)
			if(e=='@')
			{
				cmake_reserved=true;
				result_filename.push_back('_');
			}
			else
				result_filename.push_back(e);
		if(cmake_reserved)
		{
			print(cmake_file1,"add_library(",result_filename,".",exec_charset_name," SHARED src/i18n_data/locale/",fnm,".cc)\n",
			"set_target_properties(",result_filename,".",exec_charset_name," PROPERTIES OUTPUT_NAME ",fnm,".",exec_charset_name,")\n");
			print(cmake_file2,"target_compile_options(",result_filename,".",exec_charset_name," PRIVATE -fexec-charset=",exec_charset_name,")\n");
			print(cmake_file3,"target_compile_options(",result_filename,".",exec_charset_name," PRIVATE /execution-charset:",exec_charset_name,")\n");
		}
		else
		{
			print(cmake_file1,"add_library(",fnm,".",exec_charset_name," SHARED src/i18n_data/locale/",fnm,".cc)\n");
			print(cmake_file2,"target_compile_options(",fnm,".",exec_charset_name," PRIVATE -fexec-charset=",exec_charset_name,")\n");
			print(cmake_file3,"target_compile_options(",fnm,".",exec_charset_name," PRIVATE /execution-charset:",exec_charset_name,")\n");

#if 0
			print(cmake_file2,"install(TARGETS ",fnm,".",exec_charset_name," LIBRARY DESTINATION lib/fast_io_i18n_data/locale)\n");
#endif
		}
#if 0
		print(cmake_file3,"add_library(",result_filename,".",exec_charset_name," SHARED src/i18n_data/locale/",fnm,".cc)\n",
		"set_target_properties(",result_filename,".",exec_charset_name," PROPERTIES OUTPUT_NAME ",fnm,".",exec_charset_name,")\n");
#endif
		print(cmake_file4,"install(TARGETS ",result_filename,".",exec_charset_name," LIBRARY DESTINATION lib/fast_io_i18n_data/locale)\n");
	}
	print(fast_io::out(),"done\n");
}