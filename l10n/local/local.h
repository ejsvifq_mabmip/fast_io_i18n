#pragma once

#include<cstddef>
#include<new>
#include<type_traits>
#include<concepts>

namespace fast_io_i18n
{

[[noreturn]] inline void fast_terminate() noexcept
{
#ifndef FAST_IO_NOT_TERMINATE
//https://llvm.org/doxygen/Compiler_8h_source.html

#if defined(_MSC_VER)
	__debugbreak();
#else
#if __has_builtin(__builtin_trap)
	__builtin_trap();
#else
	//create a null pointer hardware exception to terminate the program
	*(reinterpret_cast<char volatile*>(0))=0;
#endif
#endif
#endif
}

extern "C" void* _aligned_malloc(std::size_t,std::size_t) noexcept;

extern "C" void* _aligned_free(void*) noexcept;

extern "C" void* _aligned_realloc(void*,std::size_t,std::size_t) noexcept;

inline void* aligned_realloc_noexcept(void* ptr,std::size_t sz) noexcept
{
	ptr=_aligned_realloc(ptr,sz,alignof(std::max_align_t));
	if(ptr==nullptr)
		fast_terminate();
	return ptr;
}

}

inline void* operator new(std::size_t size,std::align_val_t al) noexcept
{
	if(size==0)
		fast_io_i18n::fast_terminate();
	auto ptr{fast_io_i18n::_aligned_malloc(size,static_cast<std::size_t>(al))};
	if(ptr==nullptr)
		fast_io_i18n::fast_terminate();
	return ptr;
}

inline void* operator new[](std::size_t size,std::align_val_t al) noexcept
{
	return operator new(size,al);
}

inline void* operator new(std::size_t size) noexcept
{
	return operator new(size,std::align_val_t{alignof(std::max_align_t)});
}

inline void* operator new[](std::size_t size) noexcept
{
	return operator new(size);
}

inline void operator delete(void* ptr) noexcept
{
	if(ptr==nullptr)
		return;
	fast_io_i18n::_aligned_free(ptr);
}

inline void operator delete[](void* ptr) noexcept
{
	operator delete(ptr);
}

inline void operator delete(void* ptr,std::align_val_t) noexcept
{
	operator delete(ptr);
}

inline void operator delete[](void* ptr,std::align_val_t) noexcept
{
	operator delete(ptr);
}

namespace fast_io_i18n
{

template<typename T>
struct noexcept_aligned_malloc_allocator
{
	using value_type = T;
	using size_type = std::size_t;
	using difference_type = std::ptrdiff_t;
	using propagate_on_container_move_assignment = std::true_type;
	using is_always_equal = std::true_type;
	[[nodiscard]] constexpr T* allocate(std::size_t n) noexcept
	{
		if(std::is_constant_evaluated())
			return new T[n];
		else
//ignore max size mul overflow since we will never reach that
			return reinterpret_cast<T*>(operator new(n*sizeof(T)));
	}
	[[nodiscard]] T* allocate(std::size_t n,void* hint) noexcept
	{
//ignore max size mul overflow since we will never reach that
		return reinterpret_cast<T*>(fast_io_i18n::aligned_realloc_noexcept(hint,n*sizeof(T)));
	}
	constexpr void deallocate(T* p, std::size_t n) noexcept
	{
		if(std::is_constant_evaluated())
			delete[] p;
		else
			operator delete(p,n*sizeof(T));
	}
};

template<typename T1,typename T2>
constexpr bool operator==(noexcept_aligned_malloc_allocator<T1>,noexcept_aligned_malloc_allocator<T2>) noexcept
{
	return true;
}

}