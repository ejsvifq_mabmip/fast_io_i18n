#include"local.h"
#include<cstring>
#if 0
#include<fast_io.h>
#endif

namespace fast_io_i18n
{

template<typename T>
struct iovec_string_base
{
	T* base{};
	std::size_t len{};
};

template<typename T>
inline constexpr std::size_t my_strlen(T const* t) noexcept
{
	std::size_t sz{};
	for(;*t;++sz)
		++t;
	return sz;
}

template<typename T>
struct iovec_string:iovec_string_base<T>
{
	constexpr iovec_string()=default;
	constexpr iovec_string(T const* data,std::size_t sz)
	{
		this->base=new T[sz+1];
		this->len=sz;
		if(sz)
			std::memcpy(this->base,data,sz*sizeof(T));
		this->base[sz]=0;
	}
	constexpr iovec_string(T const* cstr):iovec_string(cstr,my_strlen(cstr)){}
	template<std::size_t N>
	constexpr iovec_string(T const (&data)[N]):iovec_string(data,N-1){}

	constexpr iovec_string(iovec_string const&)=delete;
	constexpr iovec_string& operator=(iovec_string const&)=delete;
	constexpr iovec_string(iovec_string &&other) noexcept:iovec_string_base<T>{other.base,other.len}
	{
		other.base=nullptr;
		other.len=0;
	}
	iovec_string& operator=(iovec_string &&other) noexcept
	{
		if(&other==this)
			return *this;
		delete this->base;
		this->base=other.base;
		this->len=other.len;
		return *this;
	}
	constexpr ~iovec_string()
	{
		delete[] this->base;
	}
};

#if 0
template<typename T>
inline constexpr fast_io::basic_io_scatter_t<T> print_alias_define(fast_io::io_alias_t,iovec_string_base<T> base) noexcept
{
	return {base.base,base.len};
}
#endif

}

int main()
{
	fast_io_i18n::iovec_string<char> vec("Hello");
}