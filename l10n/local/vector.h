#pragma once
#include<concepts>
#include<ranges>
#include<iterator>
#include<compare>

namespace fast_io_i18n
{
/*
Unfortunately, std::vector will throw length_error we cannot use it.
*/
template<std::movable T,typename Allocator=noexcept_aligned_malloc_allocator<T>>
class vector
{
public:
	using value_type = T;
	using allocator_type = Allocator;
	using allocator_traits = std::allocator_traits<allocator_type>;
	using difference_type = typename allocator_traits::difference_type;
	using size_type = typename allocator_type::size_type;
	using reference = T&;
	using const_reference = T const&;
	using pointer = typename allocator_type::pointer;
	using const_pointer = typename allocator_type::const_pointer;
	using iterator = pointer;
	using const_iterator = const_pointer;
	using reverse_iterator = std::reverse_iterator<iterator>;
	using const_reverse_iterator = std::reverse_iterator<const_iterator>;
	pointer bg{},ed{},cap{};
	[[no_unique_address]] allocator_type alloc;
	constexpr vector() noexcept(noexcept(allocator_type()))=default;
	constexpr allocator_type get_allocator() const noexcept
	{
		return alloc;
	}
	constexpr reference operator[](size_type pos) noexcept
	{
		return bg[pos];
	}
	constexpr const_reference operator[](size_type pos) const noexcept
	{
		return bg[pos];
	}
	constexpr size_type size() const noexcept
	{
		return static_cast<std::size_t>(ed-bg);
	}
	constexpr bool empty() const noexcept
	{
		return bg==ed;
	}
	constexpr void clear() noexcept
	{
		for(;ed!=bg;allocator_traits::destroy(alloc,--ed));
	}
	constexpr size_type capacity() const noexcept
	{
		return static_cast<std::size_t>(cap-bg);
	}
	constexpr size_type max_size() const noexcept
	{
		return allocator_traits::max_size(alloc);
	}
	constexpr const_reference back() const noexcept
	{
		return ed[-1];
	}
	constexpr reference back() noexcept
	{
		return ed[-1];
	}
	constexpr const_reference front() const noexcept
	{
		return *bg;
	}
	constexpr reference front() noexcept
	{
		return *bg;
	}
	constexpr void pop_back() noexcept
	{
		allocator_traits::destroy(alloc,--ed);
	}
	template<typename... Args>
	requires std::constructible_from<T,Args...>
	constexpr reference emplace_back_unchecked(Args&& ...args)
	{
		allocator_traits::construct(alloc,ed,std::forward<Args>(args)...);
		return ed[-1];
	}
	constexpr iterator begin() noexcept
	{
		return bg;
	}
	constexpr const_iterator begin() const noexcept
	{
		return bg;
	}
	constexpr const_iterator cbegin() const noexcept
	{
		return bg;
	}

	constexpr iterator end() noexcept
	{
		return ed;
	}
	constexpr const_iterator end() const noexcept
	{
		return ed;
	}
	constexpr const_iterator cend() noexcept
	{
		return ed;
	}

	constexpr reverse_iterator rbegin() noexcept
	{
		return reverse_iterator(end());
	}
	constexpr const_reverse_iterator rbegin() const noexcept
	{
		return reverse_iterator(cend());
	}
	constexpr const_reverse_iterator crbegin() const noexcept
	{
		return reverse_iterator(cend());
	}
	constexpr reverse_iterator rend() noexcept
	{
		return reverse_iterator(begin());
	}
	constexpr const_reverse_iterator rend() const noexcept
	{
		return reverse_iterator(cbegin());
	}
	constexpr const_reverse_iterator crend() const noexcept
	{
		return reverse_iterator(cbegin());
	}

private:
	struct guard
	{
		pointer b;
		pointer e;
		pointer c;
		allocator_type& a;
		constexpr ~guard()
		{
			if(b)
			{
				for(;e!=b;allocator_traits::destroy(a,--e));
				allocator_traits::deallocate(a,static_cast<std::size_t>(c-b));
			}
		}
	};
	constexpr void grow_unchecked_size(size_type sz)
	{
		auto ptr{allocator_traits::allocate(alloc,sz,bg)};
		guard g{ptr,ptr,ptr+sz,alloc};
		for(auto i{bg};i!=ed;++i)
		{
			allocator_traits::construct(alloc,g.e,std::move(*i));
			++g.e;
		}
		g.b=bg;
		g.e=ed;
		g.c=cap;
	}
	constexpr void grow_size(size_type size)
	{
		constexpr std::size_t mxsz{max_size()};
		if(mxsz<size)
			fast_terminate();
		grow_unchecked_size(size);
	}
	constexpr void grow_twice()
	{
		size_type new_cap{capacity()};
		if(new_cap==0)
			new_cap=1;
		constexpr std::size_t mxsz{max_size()};
		constexpr std::size_t mxszdiv2{mxsz/2};
		if(mxszdiv2<new_cap)
			fast_terminate();
		grow_unchecked_size(new_cap<<1);
	}
	template<typename... Args>
	requires std::constructible_from<T,Args...>
	constexpr reference emplace_back_grow_impl(Args&& ...args)
	{
		grow_twice();
		return emplace_back_unchecked(std::forward<Args>(args)...);
	}
public:
	template<typename... Args>
	requires std::constructible_from<T,Args...>
	constexpr reference emplace_back(Args&& ...args)
	{
		if(ed==cap)
			return emplace_back_grow_impl(std::forward<Args>(args)...);
		return emplace_back_unchecked(std::forward<Args>(args)...);
	}
	constexpr void push_back_unchecked(value_type&& t)
	{
		emplace_back_unchecked(std::move(t));
	}
	constexpr void push_back(value_type&& t)
	{
		emplace_back(std::move(t));
	}
	constexpr void push_back_unchecked(value_type&& t)
	{
		emplace_back_unchecked(t);
	}
	constexpr void push_back(value_type const& t)
	{
		emplace_back(t);
	}
private:
	constexpr void destroy_container()
	{
		clear();
		if(bg)
			allocator_traits::deallocate(alloc,bg,static_cast<std::size_t>(cap-bg));
	}
public:
	constexpr ~vector()
	{
		destroy_container();
	}

	constexpr vector(vector const& other)
	{
	}

	constexpr vector& operator=(vector const& other)
	{
	}

	constexpr vector(vector&& other) noexcept
	{
	}

	constexpr vector& operator=(vector&& other) noexcept
	{
	}

private:
	constexpr iterator erase_range_impl(iterator first,iterator last)
	{
		std::size_t fs_to_ls(last-first);
		std::size_t ls_to_ed(ls-ed);
		if(ls_to_ed<fs_to_ls)
		{
			auto ret{std::move(last,ed,first)};
			for(;ed!=ret;allocator_traits::destroy(alloc,--ed));
			return ed;
		}
		else
		{
			auto ret{std::move(last,last+fs_to_ls,first)};
			for(;ed!=ret;allocator_traits::destroy(alloc,--ed));
			return ed;
		}
	}
	constexpr iterator erase_single_element_impl(iterator first)
	{
		std::move(first+1,ed,first);
		pop_back();
		return first;
	}
public:
	constexpr iterator erase(const_iterator pos)
	{
		return erase_single_element_impl(pos-cbegin()+bg);
	}
	constexpr iterator erase(const_iterator first,const_iterator last)
	{
		return erase_range_impl(first-cbegin()+bg,last-cbegin()-bg);
	}
};

template<typename T,typename Alloc>
inline constexpr bool operator==(vector<T,Alloc> const& lhs,vector<T,Alloc> const& rhs)
{
	if(lhs.size()!=rhs.size())
		return false;
	return std::equal(lhs.cbegin(),lhs.cend(),rhs.cbegin(),rhs.cend());
}

template<typename T,typename Alloc>
inline constexpr auto operator<=>(vector<T,Alloc> const& lhs,vector<T,Alloc> const& rhs)
{
	return std::lexicographical_compare_three_way(lhs.begin(), lhs.end(),rhs.begin(), rhs.end());
}

template<typename T,typename Alloc,typename U>
constexpr typename vector<T,Alloc>::size_type erase(vector<T,Alloc>& c,U const& value)
{
	auto it{std::remove(c.begin(), c.end(), value)};
	auto r{c.end()-it};
	c.erase(it, c.end());
	return r;
}

template<typename T,typename Alloc,typename Pred>
constexpr typename vector<T,Alloc>::size_type erase_if(vector<T,Alloc>& c,Pred pred)
{
	auto it{std::remove_if(c.begin(), c.end(), pred)};
	auto r{c.end()-it};
	c.erase(it, c.end());
	return r;
}
template<typename T,typename Alloc>
constexpr void swap(vector<T,Alloc>& lhs,vector<T,Alloc>& rhs) noexcept(noexcept(lhs.swap(rhs)))
{
	lhs.swap(rhs);
}
}