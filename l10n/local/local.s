	.file	"local.cc"
	.text
	.def	__main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
.LC0:
	.ascii "Hello\0"
	.section	.text.unlikely,"x"
.LCOLDB1:
	.section	.text.startup,"x"
.LHOTB1:
	.globl	main
	.def	main;	.scl	2;	.type	32;	.endef
	.seh_proc	main
main:
	pushq	%rbx
	.seh_pushreg	%rbx
	subq	$32, %rsp
	.seh_stackalloc	32
	.seh_endprologue
	call	__main
	xorl	%ebx, %ebx
	leaq	.LC0(%rip), %rax
.L2:
	movq	%rbx, %rcx
	addq	$1, %rbx
	cmpb	$0, (%rax,%rbx)
	jne	.L2
	addq	$2, %rcx
	je	.L4
	movl	$16, %edx
	call	_aligned_malloc
	movq	%rax, %rcx
	testq	%rax, %rax
	je	.L4
	testq	%rbx, %rbx
	jne	.L14
.L5:
	movb	$0, (%rcx,%rbx)
	call	_aligned_free
	xorl	%eax, %eax
	addq	$32, %rsp
	popq	%rbx
	ret
.L14:
	movq	%rbx, %r8
	leaq	.LC0(%rip), %rdx
	call	memcpy
	movq	%rax, %rcx
	jmp	.L5
	.seh_endproc
	.section	.text.unlikely,"x"
	.def	main.cold;	.scl	3;	.type	32;	.endef
	.seh_proc	main.cold
	.seh_stackalloc	40
	.seh_savereg	%rbx, 32
	.seh_endprologue
main.cold:
.L4:
	ud2
	.section	.text.startup,"x"
	.section	.text.unlikely,"x"
	.seh_endproc
.LCOLDE1:
	.section	.text.startup,"x"
.LHOTE1:
	.ident	"GCC: (GCC with MCF thread model, built by cqwrteur.) 11.0.0 20201114 (experimental)"
	.def	_aligned_malloc;	.scl	2;	.type	32;	.endef
	.def	_aligned_free;	.scl	2;	.type	32;	.endef
	.def	memcpy;	.scl	2;	.type	32;	.endef
