#include<fast_io_device.h>
#include<fast_io.h>

template<bool special=false>
inline void print_category(fast_io::u8obuf_file& u8obf,std::u8string_view category_name,std::u8string_view u8strvw)
{
	print(u8obf,u8R"raw_stop(
template<std::integral char_type>
inline constexpr bool is_c_)raw_stop",category_name,u8R"raw_stop((char_type ch) noexcept
{
	if constexpr(std::same_as<char,char_type>)
switch(ch){)raw_stop");
	for(auto const ch : u8strvw)
	{
		if constexpr(special)
		{
			if(ch==u8'\"'||ch==u8'\\'||ch=='\'')
				print(u8obf,u8"case \'\\",fast_io::chvw(ch),u8"\':");
			else
				print(u8obf,u8"case \'",fast_io::chvw(ch),u8"\':");
		}
		else
			print(u8obf,u8"case \'",fast_io::chvw(ch),u8"\':");
	}
	print(u8obf,u8"return true;default:return false;}\n"
	"\telse if constexpr(std::same_as<wchar_t,char_type>)\nswitch(ch){");
	for(auto const ch : u8strvw)
	{
		if constexpr(special)
		{
			if(ch==u8'\"'||ch==u8'\\'||ch=='\'')
				print(u8obf,u8"case L\'\\",fast_io::chvw(ch),u8"\':");
			else
				print(u8obf,u8"case L\'",fast_io::chvw(ch),u8"\':");
		}
		else
			print(u8obf,u8"case L\'",fast_io::chvw(ch),u8"\':");
	}
	print(u8obf,u8"return true;default:return false;}\n"
	"\telse\nswitch(ch){");
	for(auto const ch : u8strvw)
	{
		if constexpr(special)
		{
			if(ch==u8'\"'||ch==u8'\\'||ch=='\'')
				print(u8obf,u8"case u8\'\\",fast_io::chvw(ch),u8"\':");
			else
				print(u8obf,u8"case u8\'",fast_io::chvw(ch),u8"\':");
		}
		else
			print(u8obf,u8"case u8\'",fast_io::chvw(ch),u8"\':");
	}
	print(u8obf,u8"return true;default:return false;}\n}\n");
}

inline void print_to_category(fast_io::u8obuf_file& u8obf,std::u8string_view category_name,std::u8string_view u8strvw,std::u8string_view u8strvw1)
{
	print(u8obf,u8R"raw_stop(
template<std::integral char_type>
inline constexpr char_type to_c_)raw_stop",category_name,u8R"raw_stop((char_type ch) noexcept
{
	if constexpr(std::same_as<char,char_type>)
switch(ch){)raw_stop");
	for(std::size_t i{};i!=u8strvw.size();++i)
	{
		auto const ch{u8strvw[i]};
		print(u8obf,u8"case \'",fast_io::chvw(ch),u8"\':return \'",fast_io::chvw(u8strvw1[i]),u8"\';");
	}
	print(u8obf,u8"default:return ch;}\n"
	"\telse if constexpr(std::same_as<wchar_t,char_type>)\nswitch(ch){");
	for(std::size_t i{};i!=u8strvw.size();++i)
	{
		auto const ch{u8strvw[i]};
		print(u8obf,u8"case L\'",fast_io::chvw(ch),u8"\':return L\'",fast_io::chvw(u8strvw1[i]),u8"\';");
	}
	print(u8obf,u8"default:return ch;}\n"
	"\telse\nswitch(ch){");
	for(std::size_t i{};i!=u8strvw.size();++i)
	{
		auto const ch{u8strvw[i]};
		print(u8obf,u8"case u8\'",fast_io::chvw(ch),u8"\':return u8\'",fast_io::chvw(u8strvw1[i]),u8"\';");
	}
	print(u8obf,u8"default:return ch;}\n}\n");
}

int main()
{
	fast_io::u8obuf_file u8obf("is_everything.h");
	using namespace std::string_view_literals;
	print_category(u8obf,u8"alnum",u8"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"sv);
	print_category(u8obf,u8"alpha",u8"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"sv);
	print_category(u8obf,u8"lower",u8"abcdefghijklmnopqrstuvwxyz"sv);
	print_category(u8obf,u8"upper",u8"ABCDEFGHIJKLMNOPQRSTUVWXYZ"sv);
	print_category(u8obf,u8"digit",u8"0123456789"sv);
	print_category(u8obf,u8"xdigit",u8"0123456789abcdefABCDEF"sv);
	print_category<true>(u8obf,u8"punct",u8"!\"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~"sv);
	print_category<true>(u8obf,u8"graph",u8"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!\"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~"sv);
	print_category<true>(u8obf,u8"print",u8"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!\"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~ "sv);
	print_to_category(u8obf,u8"lower",u8"ABCDEFGHIJKLMNOPQRSTUVWXYZ",u8"abcdefghijklmnopqrstuvwxyz");
	print_to_category(u8obf,u8"upper",u8"abcdefghijklmnopqrstuvwxyz",u8"ABCDEFGHIJKLMNOPQRSTUVWXYZ");
}