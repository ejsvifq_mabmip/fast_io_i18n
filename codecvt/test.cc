#include<fast_io.h>
#include<fast_io_device.h>
#include"gb18030.h"

int main()
{
	std::array<char,100> array;
	fast_io::u32to_gb18030_state_t state;
	using namespace std::string_view_literals;
	constexpr std::u32string_view strvw(U"我是好人 😂 🤣🀄🀅🀏 ☭ 🚄🚄🚄🚄"sv);
	fast_io::obuf_file obf("gb18030_res.txt");
	write(obf,array.begin(),
	decorate_reserve_define(fast_io::io_reserve_type<char,fast_io::u32to_gb18030_state_t>,state,
	strvw.begin(),strvw.end(),array.begin()));
/*
	constexpr char32_t code_point{U'我'};
	std::size_t sz{fast_io::get_gb18030_code_units(code_point,reinterpret_cast<char*>(array.data()))};
	fast_io::c_io_observer_unlocked stdout_unlocked{stdout};
	println(stdout_unlocked,code_point," sz=",sz);
	for(std::size_t i{};i!=sz;++i)
	{
		if(i)
			print(stdout_unlocked,",");
		print(stdout_unlocked,fast_io::manipulators::hex_upper(array[i]));
	}*/

}