#include<fast_io.h>
#include<fast_io_device.h>
#include"gb18030.h"
#include<fast_io_driver/posix_iconv.h>
#include<vector>


constexpr char32_t utf32cp_by_gb18030_index_test(char32_t index) noexcept
{
	using namespace fast_io::details::utf2::gb18030;
	constexpr char32_t maximum_index{linear(0x90308130)};
	if(index<=maximum_index)[[likely]]
	{
		char32_t sum{linear_18030_base};
		for(std::size_t i{};i!=13;++i)
		{
			auto const& e{gb18030_ranges[i]};
			auto const e0{e[0]};
			auto const e1{e[1]};
			sum+=static_cast<char32_t>(e1-e0);
			auto const e2{e[2]};
			auto const e3{e[3]};
			char32_t diff{static_cast<char32_t>(e3-e2)};
			if(static_cast<char32_t>(index-e2)<=diff)
				return -1;
			else if(e3<index&&index<gb18030_ranges[i+1][2])
				return index-sum;	
		}
	}
	return -1;
}

int main()
{
	using namespace fast_io::manipulators;
	using namespace fast_io::details::utf2::gb18030;
	fast_io::posix_iconv_file picf("GB18030","UTF-32LE");
	std::string str;
	fast_io::obuf_file obf("gb18030_table.txt");
	fast_io::obuf_file obf_result("gb18030_tb.h");
	char32_t pos{0};
	char32_t max_2v{};
	char32_t max_4v{};
	std::vector<std::pair<char32_t,char32_t>> vec;
	std::vector<std::pair<char32_t,char32_t>> vec2;
	for(std::size_t i{};i!=13;++i)
	{
		char32_t j{128};
		if(i)
			j=gb18030_ranges[i-1][1];
		for(;j!=*gb18030_ranges[i];++j)
		{
			std::u32string_view u32view(&j,1);
			fast_io::ostring_ref ref{str};
			print(obf,pos,"(0x",hex_upper(pos),")\t",j,"(0x",hex_upper(j),")\ti=",i);
			print(ref,iconv_code_cvt(picf,u32view));
			if(str.empty())
			{
				print(obf," invalid\n");
				print(obf_result,",0x37A43184");
			}
			else
			{
				print(obf," size:",str.size());
				char32_t v{};
				for(char8_t ch: str)
				{
					print(obf,"\t0x",hex_upper(ch));
					v=(v<<8)|ch;
				}
				if((v&0xFFFF)==v)
				{
					if(max_2v<v)
						max_2v=v;
					vec2.push_back({v,j});
					v=fast_io::details::byte_swap(static_cast<char16_t>(v));
				}
				else
				{
					if(max_4v<v)
						max_4v=v;
					vec.push_back({v,j});
					v=fast_io::details::byte_swap(v);
				}
				println(obf,"\tv:0x",hex_upper(v));
				if(pos==0)
					print(obf_result,"0x",hex_upper(v));
				else
					print(obf_result,",0x",hex_upper(v));
			}
			str.clear();
			reset_state(picf);
			++pos;
		}	
	}
	print(obf,"max_2v:",max_2v,"(0x",hex_upper(max_2v),") ",max_4v,"(0x",hex_upper(max_4v),")\n");
	fast_io::obuf_file obf2("gb18030_table_rev.txt");
	fast_io::obuf_file obf3("gb18030_uni4_tb.h");
	std::size_t count{};
	for(std::size_t i{};i!=vec.size();++i)
	{

		auto e{vec[i]};
		auto index{utf32cp_by_gb18030_index_test(linear(e.first))};
		if(index!=static_cast<char32_t>(-1))
		print(obf2,"i=",i," gb18030:",e.first,"(0x",hex_upper(e.first),") unicode:",e.second,"(0x",hex_upper(e.second),") linear:",
		linear(e.first),"(0x",hex_upper(linear(e.first)),") index:",
		index,"(0x",
		hex_upper(index),")\n");
		if(820==i)
		{
			print(obf3,"0x",hex_upper(e.second));
			++count;
		}
		else if(820<i)
		{
			if(index==static_cast<char32_t>(-1))
				print(obf3,",0xFFFD");
			else
				print(obf3,",0x",hex_upper(e.second));
			++count;
		}
	}
	println(obf2,"total count:",count);
	fast_io::posix_iconv_file picf2("UTF-32LE","GB18030");
	fast_io::obuf_file obf4("gb18030_table_uni2_result.txt");
	fast_io::obuf_file obf5("gb18030_uni2_tb.h");
	std::u32string strbuffer;
	for(char8_t i{0x81};i!=0xFF;++i)
	{
		for(char8_t j{0x40};j!=0xFF;++j)
		{
			if(j!=0x7F)[[likely]]
			{
				strbuffer.clear();
				reset_state(picf2);
				char buffer[2]={static_cast<char>(i),static_cast<char>(j)};
				std::string_view view(buffer,2);
				fast_io::ostring_ref ref{strbuffer};
				print(ref,iconv_code_cvt(picf2,view));
				print(obf4,"0x",hex_upper(i)," 0x",hex_upper(j)," ",strbuffer.size());
				if(strbuffer.size()==1)
				{
					println(obf4," 0x",hex_upper(strbuffer.front()));
					if(i!=0x81||j!=0x40)
						print(obf5,",");
					print(obf5,"0x",hex_upper(strbuffer.front()));
				}
				else
					print(obf4," not exist\n");
			}
		}
	}
}