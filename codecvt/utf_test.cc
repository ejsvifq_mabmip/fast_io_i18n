#include<fast_io.h>
#include"general.h"

auto utf_code_cvt_test(char16_t const* src_first,char16_t const* src_last,char* __restrict__ dst) noexcept
{
	return fast_io::details::gb18030_code_cvt(src_first,src_last,dst);
}

/*
int main()
{}
*/