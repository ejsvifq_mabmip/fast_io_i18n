#include<fast_io_device.h>
#include<fast_io.h>

inline void do_thing(fast_io::cstring_view infile,fast_io::cstring_view outfile,bool hex=true)
{
	using namespace fast_io::manipulators;
	fast_io::ibuf_file ibf(infile);
	fast_io::obuf_file obf(outfile);
	for(std::size_t i{};i!=256;++i)
	{
		char8_t ch;
		scan(ibf,hex_get(ch));
		if(i)
			print(obf,",");
		if(hex)
			print(obf,"0x",hex_upper(ch));
		else
			print(obf,ch);
	}
}

int main()
{
	do_thing("bm_i8_to_ebcdic.txt","bm_i8_to_ebcdic.h");
	do_thing("bm_ebcdic_to_i8.txt","bm_ebcdic_to_i8.h");
	do_thing("utfebcdic_shadow.txt","utfebcdic_shadow.h",false);
}